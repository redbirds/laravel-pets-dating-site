<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
*/
Route::get('/', function () { return view('welcome'); });

/** 0.0) ERROR ROUTES `404` and `405`.
For redirect on View with special content
*/
//Route::get('404',['uses'=>'ErrorHandlerController@errorCode404'])->name('404');
//Route::get('405',['uses'=>'ErrorHandlerController@errorCode405'])->name('405');

/** 0) FOR LOGIN / REGISTERED
*/
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


/** 1) FOR FRONTEND part.
*/
Route::group( ['middleware'=>['web'] ], function() {

    /** 1.1__________________Home page show - For:'/' */
    Route::resource('/', 'IndexResourceController', [
        'only'=>['index', 'store'],
        'names'=>['index'=>'home']
    ]);

    /** 1.2__________________About us page show - For:'/about-us' */
    Route::resource('/about-us', 'AboutusResourceController', [
        'only'=>['index'],
        'names'=>['index'=>'about_us']
    ]);

    /** 1.3__________________Contacts page show - For:'/contacts' */
    Route::resource('/contacts', 'ContactsResourceController', [
        'only'=>['index'],
        'names'=>['index'=>'contacts']
    ]);

    /** 1.4__________________Articles page show - For:'/articles' */
    Route::resource('/articles', 'ArticleResourceController', [
        'names'=>['index'=>'articles', 'show'=>'single_articles'],
        /*
        'parameters' => [   //For:'/articles/{alias}' (alias = article1,article2,article3...)
        'articles' => 'alias' //переименовываем стандартное имя для такого маршрута 'articles' на 'alias'
        ]
        */
    ]);
        /* For:'//articles/cat/{cat}' */
        Route::get('/articles/cat/{cat?}', ['uses'=>'ArticleResourceController@index', 'as'=>'articles_cat']);

        //Route::post('/search', ['uses'=>'SearchController@index', 'as'=>'search']);
        Route::match(['get','post'], '/search', ['uses'=>'SearchController@index'])->name('search');

});
