<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexResourceController extends SiteMainController
{
    public function __construct() {
        parent::__construct();
        $this->_template_view_name = 'frontendsite.'.env('THEME').'.index';
        $this->_bar_for_template_view = FALSE;
        $this->rightbar_for_template_view = FALSE;
        $this->_leftbar_for_template_view = FALSE;

        $this->_keywords = 'Home Page, Lapki, LITTUS';
        $this->_meta_description = 'Home Page description text ...';
        $this->_title= 'HOME';
    }
    //__________________________________________________________________________________________________________________________________________________________

    /** Display a listing of the resource.
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //=> GET DATA(from DB) THROUGH the MODEL:

        //=> FORMING THE MAIN ARRAY with DATA FOR THE TEMPLATE:
        $this->_vars_for_template_view['show_controller_info'] = $this->show_controller_info;

        //=> FORMING Right Side-bar шаблона `resources/views/frontendsite/pink/index.blade.php`
//        if( $this->_bar_for_template_view ) {  //если это св-во родит.Контроллера переопределено как TRUE, значит что хотим использовать и формировать Side-bar
//            $this->_rightbar_for_template_view = view('frontendsite.'.env('THEME').'.include._right_side_bar')
//                ->with( 'portfolios', SiteMainController::get_entries_limit( $get_portfolio_side_bar, $configLimitPortfoliosShowSideBar,true) ) //Данные для Portfolios для Side-bar
//                ->with( 'articles', SiteMainController::get_entries_limit( $get_articles_side_bar, $configLimitArticlesShowSideBar,true ) ); //Данные для Articles для Side-bar
//        }


        $test = 'Test string ++ ......';
        //=> FORMING dynamic template section `resources/views/frontendsite/pink/index.blade.php` - "content" for "HOME" page
        $content_page = view('frontendsite.'.env('THEME').'.include._home')
            ->with( 'test', $test )
            ->with( 'right_sidebar_content', $this->_rightbar_for_template_view ); //The rendered View with the right site bar and the data for it

        //=> RENDER View and DATA for View
        $this->_vars_for_template_view['page_content'] = $content_page;
        return $this->renderOutput();
    }


    /** Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response

    public function store(Request $request)
    {
        if( $request->isMethod('post') ) {
            $data_of_post = $request->all();
            dd($data_of_post);

            //return redirect()->route('contacts');
        }

    } //__/public function store()
*/
} //__/class IndexResourceController
