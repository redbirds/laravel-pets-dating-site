<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchController extends SiteMainController
{
    public function __construct() {
        parent::__construct();
        $this->_template_view_name = 'frontendsite.'.env('THEME').'.index';
        $this->_bar_for_template_view = FALSE;
        $this->rightbar_for_template_view = FALSE;
        $this->_leftbar_for_template_view = FALSE;

        $this->_keywords = 'Search result Page, Lapki, LITTUS';
        $this->_meta_description = 'Search result Page description text ...';
        $this->_title= 'SEARCH';
    }
    //__________________________________________________________________________________________________________________________________________________________

    /** Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $data_of_post = FALSE;
        $data_of_get = FALSE;

        if( $request->isMethod('get') ) {
            $data_of_get = $request->query('id');
            //dump( $request->query('id') ); //dump( $request->query() );
        }


        if( $request->isMethod('post') ) {
            $keyword_search = false;
            $city_search = false;
            $is_desktop_transfer = ''; //Check Post data transferred from Desktop version or Mobile version

            //$data_of_post = $request->all(); //dump($data_of_post);
            $data_of_post = $request->except('_token');

            //return redirect()->route('contacts');
        }

        //=> GET DATA(from DB) THROUGH the MODEL:i

        //=> FORMING THE MAIN ARRAY with DATA FOR THE TEMPLATE:
        $this->_vars_for_template_view['show_controller_info'] = $this->show_controller_info;

        //=> FORMING dynamic template section `resources/views/frontendsite/pink/index.blade.php` - "content" for "SEARCH" page

        $content_page = view('frontendsite.'.env('THEME').'.include._search')
             ->with( 'data_of_post', $data_of_post )
            ->with( 'data_of_get', $data_of_get );

        //=> RENDER View and DATA for View
        $this->_vars_for_template_view['page_content'] = $content_page;
        return $this->renderOutput();
    }

} //__/class SearchController
