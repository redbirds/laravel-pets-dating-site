<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchController extends SiteMainController
{
    public function __construct() {
        parent::__construct();
        $this->_template_view_name = 'frontendsite.'.env('THEME').'.index';
        $this->_bar_for_template_view = FALSE;
        $this->rightbar_for_template_view = FALSE;
        $this->_leftbar_for_template_view = FALSE;

        $this->_keywords = 'Search result Page, Lapki, LITTUS';
        $this->_meta_description = 'Search result Page description text ...';
        $this->_title= 'SEARCH';
    }
    //__________________________________________________________________________________________________________________________________________________________

    /** Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $keyword_search = false;
        $city_search = false;

        $keyword_search_mob = false;
        $pettype_search_mob = false;
        $gender_search_mob = false;
        $breed_search_mob = false;
        $city_search_mob = false;


        if( $request->isMethod('post') ) {
            $keyword_search = false;
            $city_search = false;
            $is_desktop_transfer = ''; //Check Post data transferred from Desktop version or Mobile version

            $data_of_post = $request->all(); //dump($data_of_post);

            //1.Post data transferred from Desktop version:
            if( count($data_of_post) == 3 ){
                dd($data_of_post);
                $is_desktop_transfer = '_desktop';
                $keyword_search = $data_of_post['keyword'];
                $city_search = $data_of_post['city'];
            }
            //2.Post data transferred from Mobile version:
            else{
                dd($data_of_post);
                $is_desktop_transfer = '_mobile';
                $keyword_search_mob = $data_of_post['keyword'];
                $pettype_search_mob = $data_of_post['pet_type'];
                $gender_search_mob = $data_of_post['gender'];
                $breed_search_mob = $data_of_post['breed'];
                $city_search_mob = $data_of_post['city'];
               }

            //return redirect()->route('contacts');
        }

        //=> GET DATA(from DB) THROUGH the MODEL:i

        //=> FORMING THE MAIN ARRAY with DATA FOR THE TEMPLATE:
        $this->_vars_for_template_view['show_controller_info'] = $this->show_controller_info;

        //=> FORMING dynamic template section `resources/views/frontendsite/pink/index.blade.php` - "content" for "SEARCH" page
        //1.To display the Desktop Search Page:
        if( $is_desktop_transfer ){
            $content_page = view('frontendsite.'.env('THEME').'.include._search')
                ->with( 'is_desktop_transfer', $is_desktop_transfer )
                ->with( 'keyword_search', $keyword_search )
                ->with( 'city_search', $city_search );
        }
        //2.To display the Mobile Search Page:
        else{
            $content_page = view('frontendsite.'.env('THEME').'.include._search')
                ->with( 'keyword_search_mob', $keyword_search_mob )
                ->with( 'pettype_search_mob', $pettype_search_mob )
                ->with( 'gender_search_mob', $gender_search_mob )
                ->with( 'breed_search_mob', $breed_search_mob )
                ->with( 'city_search_mob', $city_search_mob );
        }

        //=> RENDER View and DATA for View
        $this->_vars_for_template_view['page_content'] = $content_page;
        return $this->renderOutput();
    }

} //__/class SearchController
