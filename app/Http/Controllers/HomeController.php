<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /** Create a new controller instance.
     * @param Request $request
     * @return void
    */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    /** Show the application dashboard.
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('home');
        //return view('admin_home');
    }

} //__/class HomeController