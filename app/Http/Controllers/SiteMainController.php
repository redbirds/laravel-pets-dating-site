<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\App;

class SiteMainController extends Controller
{
    protected static $_objMenu;
    protected static $_objPortfolio;
    protected static $_objPortfolioFilter;
    protected static $_objSlider;
    protected static $_objArticle;
    protected static $_objComment;

    protected $_template_view_name; //name of template (View)
    public $show_controller_info = __METHOD__;

    protected $_vars_for_template_view = array();
    protected $_bar_for_template_view = FALSE;  //сheck - will there be a right site bar
    protected $_rightbar_for_template_view = FALSE; //store a rendered View with data for right site bar content
    protected $_block_popular_check = FALSE;  //сheck - will there be a block popular articles
    protected $_block_popular_content = FALSE;  //store a rendered View with data for block popular articles

    protected $_keywords;
    protected $_meta_description;
    protected $_title;

    //protected $request;
    //_______________________________________________________________________________________________________________________________________________________________________

    public function __construct(){
//        self::$_objMenu = new Menu();
//        self::$_objSlider = new Slider();
//        self::$_objPortfolio = new Portfolio();
//        self::$_objPortfolioFilter = new PortfolioFilter();
//        self::$_objArticle = new Article();
//        self::$_objComment = new Comment();
    }
    //_______________________________________________________________________________________________________________________________________________________________________

    /** Return View with data
     *  @return
    */
    protected function renderOutput() {
        if( view()->exists( $this->_template_view_name ) ) {
            return view( $this->_template_view_name, [
                'vars_for_template_view' => $this->_vars_for_template_view,
                'nav_menu' => $this->forming_nav_menu(),
                'keywords' => $this->_keywords,
                'meta_description' => $this->_meta_description,
                'title' => $this->_title,
            ] )->render();
        }
        else { abort(404); }
    }

    /** Forming Nav-Menu(parent Item Menu with sub-Item menu if it exists) for frontend-part
     *  @return  array
     */
    protected static function forming_nav_menu() {
        /* code of function */
        return $result_menu = 'Nav Menu content';
    }


} //__/class SiteMainController
