<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactsResourceController extends SiteMainController
{
    public function __construct() {
        parent::__construct();
        $this->_template_view_name = 'frontendsite.'.env('THEME').'.index';

        $this->_keywords = 'Contacts, Lapki, LITTUS';
        $this->_meta_description = 'Contacts description text ...';
        $this->_title= 'CONTACTS';
    }
    //__________________________________________________________________________________________________________________________________________________________

    /** Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //=> GET DATA(from DB) THROUGH the MODEL:

        //=> FORMING THE MAIN ARRAY with DATA FOR THE TEMPLATE:
        $this->_vars_for_template_view['show_controller_info'] = $this->show_controller_info;

        $test = 'Test string ++ ......';
        //=> FORMING dynamic template section `resources/views/frontendsite/pink/index.blade.php` - "content" for "CONTACT" page
        $content_page = view('frontendsite.'.env('THEME').'.include._contacts')
            ->with( 'test', $test );

        //=> RENDER View and DATA for View
        $this->_vars_for_template_view['page_content'] = $content_page;
        return $this->renderOutput();
    }

} //__/class ContactsResourceController
