<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

//use App\ArticleCategory;

class ArticleResourceController extends SiteMainController
{
    public function __construct() {
        parent::__construct();
        $this->_template_view_name = 'frontendsite.'.env('THEME').'.index';
        $this->_bar_for_template_view = TRUE;
        $this->_block_popular_check = TRUE;

        $this->_keywords = 'Articles Page, Lapki, LITTUS';
        $this->_meta_description = 'Articles description text ...';
        $this->_title= 'ARTICLES';
    }
    //______________________________________________________________________________________________________________________________________________________

    /** Display a listing of the resource.
     * @param bool/string $cat - parameter for URL
     * @return \Illuminate\Http\Response
    */
    public function index( $cat=FALSE ) {
        $this->show_controller_info = __FUNCTION__;
        //settings from settings.php
        $title_of_block_popular = 'Популярное';
        //______________________________________________________________________________________________________________________________________________________

        //=> GET DATA(from DB) THROUGH the MODEL:

        if( !$cat ) {
            //$get_articles_blog = SiteMainController::get_entries_with_settings(
                //SiteMainController::$_objArticle,false, $configArticlesPagination, array('users','articlesCategories', 'comments'), array()
            //); //for Articles of Blog with Pagination
        }
        else{
            //$idArticleCategory = ArticleCategory::get_cat_id($cat)->toArray()[0]['id'];
            //$get_articles_blog = SiteMainController::get_entries_with_settings(
                //SiteMainController::$_objArticle,false, $configArticlesPagination, array('users','articlesCategories', 'comments'), array('articles_category_id', '=', $idArticleCategory)
            //); //for Articles of Blog belonging to a particular (specific) category
        }

        //=> FORMING THE MAIN ARRAY with DATA FOR THE TEMPLATE:
        $this->_vars_for_template_view['show_controller_info'] = $this->show_controller_info;

        //=> FORMING dynamic template section `resources/views/frontendsite/lapki_base/index.blade.php` - "content" for "Right Side Bar" section
        if( $this->_bar_for_template_view ) {
            $this->_rightbar_for_template_view = view('frontendsite.'.env('THEME').'.include._right_side_bar')
                ->with( 'data_right_sidebar', 'fjihurhgf - data_right_sidebar' );
        }
        //=> FORMING dynamic template section `resources/views/frontendsite/lapki_base/index.blade.php` - "content" for "Block Popular Articles" section
        if( $this->_block_popular_check ) {
            $this->_block_popular_content = view('frontendsite.'.env('THEME').'.include._block_popular')
                ->with( 'name_method_info', $this->show_controller_info )
                ->with( 'data_block_popular', 'fjihurhgf cfdfde - 555525525++ - data_block_popular' )
                ->with('title_of_block_popular', $title_of_block_popular);
        }

        //=> FORMING dynamic template section `resources/views/frontendsite/lapki_base/index.blade.php` - "content" for "ARTICLES" page (BLOG)
            $content_page = view('frontendsite.'.env('THEME').'.include._articles')
                ->with( 'article_cat', $cat )
                ->with( 'name_method_info', $this->show_controller_info )
                ->with( 'right_sidebar_content', $this->_rightbar_for_template_view )
                ->with( 'right_block_popular_content', $this->_block_popular_content);

            //=> RENDER View and DATA for View
            $this->_vars_for_template_view['page_content'] = $content_page;
            return $this->renderOutput();

    } //__/public function index()


    /** Display the specified resource.
     * @param int $id - parameter for URL
     * @return \Illuminate\Http\Response
    */
    public function show( $id ) {
        $this->show_controller_info = __FUNCTION__;
         //settings from settings.php
        $title_of_block_popular = 'Похожие <span class="d-xl-inline d-lg-inline d-md-inline d-sm-none d-none">статьи</span>';
        //______________________________________________________________________________________________________________________________________________________

        //=> FORMING THE MAIN ARRAY with DATA FOR THE TEMPLATE:
        $this->_vars_for_template_view['show_controller_info'] = $this->show_controller_info;

        //=> FORMING dynamic template section `resources/views/frontendsite/lapki_base/index.blade.php` - "content" for "Right Side Bar" section
        if( $this->_bar_for_template_view ) {
            $this->_rightbar_for_template_view = view('frontendsite.'.env('THEME').'.include._right_side_bar')
                ->with( 'data_right_sidebar', 'fjihurhgf - data_right_sidebar' );
        }
        //=> FORMING dynamic template section `resources/views/frontendsite/lapki_base/index.blade.php` - "content" for "Block Popular Articles" section
        if( $this->_block_popular_check ) {
            $this->_block_popular_content = view('frontendsite.'.env('THEME').'.include._block_popular')
                ->with( 'name_method_info', $this->show_controller_info )
                ->with( 'data_block_popular', 'fjihurhgf cfdfde - 555525525++ - data_block_popular' )
                ->with('title_of_block_popular', $title_of_block_popular);;
        }

        //=> FORMING dynamic template section `resources/views/frontendsite/lapki_base/index.blade.php` - "content" for "ARTICLES" page (BLOG)
        $content_page = view('frontendsite.'.env('THEME').'.include._single_article')
            ->with( 'name_method_info', $this->show_controller_info  )
            ->with( 'right_sidebar_content', $this->_rightbar_for_template_view )
            ->with( 'right_block_popular_content', $this->_block_popular_content);

        //=> RENDER View and DATA for View
        $this->_vars_for_template_view['page_content'] = $content_page;
        return $this->renderOutput();

    } //__/public function show()


    /** Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */ public function create() {}

    /** Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */ public function store(Request $request) {}

    /** Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ public function edit($id) {}

    /** Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ public function update(Request $request, $id) {}

    /** Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ public function destroy($id) {}

} //__/class ArticleResourceController
