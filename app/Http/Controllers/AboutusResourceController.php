<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutusResourceController extends SiteMainController
{
    public function __construct() {
        parent::__construct();
        $this->_template_view_name = 'frontendsite.'.env('THEME').'.index';

        $this->_keywords = 'About us, Lapki, LITTUS';
        $this->_meta_description = 'About us description text ...';
        $this->_title= 'ABOUT US';
    }
    //__________________________________________________________________________________________________________________________________________________________

    /** Display a listing of the resource.
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //=> GET DATA(from DB) THROUGH the MODEL:

        //=> FORMING THE MAIN ARRAY with DATA FOR THE TEMPLATE:
        $this->_vars_for_template_view['show_controller_info'] = $this->show_controller_info;

        $test = 'Test string ++ ......';
        //=> FORMING dynamic template section `resources/views/frontendsite/lapki_base/index.blade.php` - "content" for "ABOUT US" page
        $content_page = view('frontendsite.'.env('THEME').'.include._about-us')
            ->with( 'test', $test );

        //=> RENDER View and DATA for View
        $this->_vars_for_template_view['page_content'] = $content_page;
        return $this->renderOutput();
    }

} //__/class AboutusResourceController
