// jQuery(document).ready(function(){ });
// if( x_X ){
//     x_X.querySelector('.accordion-custom-grid-wrap').querySelector('h1').style.color="red";
// }

/** VUE.JS */
var headerSectionDom = document.getElementById('header'); //Header Section
var footerSectionDom = document.getElementById('footer'); //Footer Section
var contactsSectionDom = document.getElementById('contacts_section'); //Contacts Page
var aboutUsSectionDom = document.getElementById('about_us_section'); //About-Us Page
var searchSectionDom = document.getElementById('home_filters_search_result'); //Search Page

if(headerSectionDom){ //#Header Section
    const appHeaderSectionDom = new Vue({
        el: '#header',
        data: function(){
            return {
                checkShow: false,
                checkShowLangPanel: false,
                checkHideShowBlockPet: false,
                checkHideShowBlockUserNav: false
            }
        },
        created: function(){
            //this.reverseMessage();
        },
        methods: {
            reverseMessage: function(){
                headerSectionDom.querySelector('.header-right').querySelector('.profile').querySelector('a').style.color="red";
            },
            hideShowBlockPet: function() {
                if( this.checkHideShowBlockPet == false ){
                    this.checkHideShowBlockPet = true;
                    this.checkHideShowBlockUserNav = false;
                }
                else { this.checkHideShowBlockPet = false; }
            },
            hideShowBlockUserNav: function() {
                if( this.checkHideShowBlockUserNav == false ){
                    this.checkHideShowBlockUserNav = true;
                    this.checkHideShowBlockPet = false;

                }
                else { this.checkHideShowBlockUserNav = false ;}
            }
        },
    });
} //__/Header Section
if(footerSectionDom){ //#Footer Section
    const appHeaderSectionDom = new Vue({
        el: '#footer',
        data: function(){
            return {
            }
        },
        created: function(){
        },
        methods: {
        },
    });
} //__/Footer Section

if(contactsSectionDom){ //#Contacts Page
    const appContactsSection = new Vue({
        el: '#contacts_section',
        data: function(){
            return {
                checkShow: false,
                show: false,
                maxValProgressBar: 10,
                message: 'Its hover text from title elemrnt `Vue.js` ' + new Date(),
                message2: 'Message 2 text content..... ',
            }
        },
        created: function(){
            //this.loadQuote();
        },
        methods: {

        },
    });
} //__/Contacts Page
else if(aboutUsSectionDom){  //#About-Us Page
    const appAboutUsSection = new Vue({
        el: '#about_us_section',
        data: function(){
            return {
                checkShow: true,
                maxValProgressBar: 10,
                message: 'Its hover text from title elemrnt `Vue.js` ' + new Date(),
                message999: 'Message 3333333333 text content..... ++',
            }
        },
        created: function(){
            this.reverseMessage();
        },
        methods: {
            reverseMessage: function(){
                //aboutUsSectionDom.querySelector('.accordion-custom-grid-wrap').querySelector('h1').style.color="red";
            }
        },
    });
} //__/About-Us Page
else if(searchSectionDom){  //#Search Page
    function changeSelectPetTypeOnLoad() {
        var elSelectPetType = document.getElementById("pet_type");
        var valueSelectPetType = elSelectPetType.value;
        var petTypeParentElement = elSelectPetType.parentElement;

        switch( valueSelectPetType ) {
            case 'dogs': petTypeParentElement.classList.add('dogs'); break;
            case 'cats': petTypeParentElement.classList.add('cats'); break;
            case 'rabbits': petTypeParentElement.classList.add('rabbits'); break;
            case 'guinea-pigs': petTypeParentElement.classList.add('guinea-pigs'); break;
            case 'ferrets': petTypeParentElement.classList.add('ferrets'); break;
            case 'chinchillas': petTypeParentElement.classList.add('chinchillas'); break;
            case 'birds': petTypeParentElement.classList.add('birds'); break;
            case 'turtles': petTypeParentElement.classList.add('turtles'); break;
            case 'horses': petTypeParentElement.classList.add('horses'); break;
        }
    } changeSelectPetTypeOnLoad();


    function changeSelectPetType() {
        var objImagesForSelectPetType = {
            dogs: 'http://lapki.test/img/shape_5@2x.png', cats: 'http://lapki.test/img/shape_3@2x.png', rabbits: 'http://lapki.test/img/shape_15@2x.png', guinea_pigs: 'http://lapki.test/img/shape_222@2x.png', ferret: 'http://lapki.test/img/shape_14@2x.png', chinchillas: 'http://lapki.test/img/shape_13@2x.png', birds: 'http://lapki.test/img/shape_12@2x.png', turtles: 'http://lapki.test/img/shape_11@2x.png', horses: 'http://lapki.test/img/shape_10@2x.png'
        };
        var elSelectPetType = document.getElementById("pet_type");
        var valueSelectPetType = elSelectPetType.value;
        var petTypeParentElement = elSelectPetType.parentElement.parentElement;

        switch( valueSelectPetType ) {
            case 'dogs': petTypeParentElement.className = ''; petTypeParentElement.classList.add('col-3','dogs'); break;
            case 'cats': petTypeParentElement.className = ''; petTypeParentElement.classList.add('col-3','cats'); break;
            case 'rabbits': petTypeParentElement.className = ''; petTypeParentElement.classList.add('col-3','rabbits'); break;
            case 'guinea-pigs': petTypeParentElement.className = ''; petTypeParentElement.classList.add('col-3','guinea-pigs'); break;
            case 'ferrets': petTypeParentElement.className = ''; petTypeParentElement.classList.add('col-3','ferrets'); break;
            case 'chinchillas': petTypeParentElement.className = ''; petTypeParentElement.classList.add('col-3','chinchillas'); break;
            case 'birds': petTypeParentElement.className = ''; petTypeParentElement.classList.add('col-3','birds'); break;
            case 'turtles': petTypeParentElement.className = ''; petTypeParentElement.classList.add('col-3','turtles'); break;
            case 'horses': petTypeParentElement.className = ''; petTypeParentElement.classList.add('col-3','horses'); break;
        } //console.log(valueSelectPetType);
    }

    const searchSectionDom = new Vue({
        el: '#home_filters_search_result',
        data: function(){
            return {
                checkShow: true,
                maxValProgressBar: 10,
                message: 'Its hover text from title elemrnt `Vue.js` ' + new Date(),
                message999: 'Message 3333333333 text content..... ++',
            }
        },
        created: function(){
            this.reverseMessage();
        },
        methods: {
            reverseMessage: function(){
                //aboutUsSectionDom.querySelector('.accordion-custom-grid-wrap').querySelector('h1').style.color="red";
            },
            clickSelectOption: function(){
            }
        },
    });
} //__/Search Page
else{
    //
}
