@extends('layouts.app')

@section('content')
<div class="container register-user-view">
    <div class="row mx-auto mb-4 panel-tab-switch-register-window">

        <?php if( Route::currentRouteName() == 'login' ):?>
        <div class="col tab-login text-center tab-active">
            <span>Вход</span>
        </div>
        <?php else:?>
        <div class="col tab-login text-center">
            <a href="{{ route("login") }}">
                <div class="w-100 h-100">
                    Вход
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if( Route::currentRouteName() == 'register' ):?>
        <div class="col tab-register text-center tab-active">
            <span>Регистрация</span>
        </div>
        <?php else:?>
        <div class="col tab-register text-center">
            <a href="{{ route("register") }}">
                <div class="w-100 h-100">
                    Регистрация
                </div>
            </a>
        </div>
        <?php endif;?>
    </div>

    <div class="row mx-auto no-gutters panel-content-window">
        <div class="col-12 align-self-center">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal form-user-auth form-register-user " method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Имя"  required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control" name="email" placeholder="E-mail" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                                <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="form-control" name="password" placeholder="Пароль" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Подтверждение пароля" required>
                        </div>

                        <div class="form-group">
                            <div class="col-12 text-center">
                                <button type="submit" name="btn_submit_register_form" id="btn_submit_register_form" class="btn btn-lg btn-danger">
                                    Регистрация
                                </button>
                            </div>
                        </div>
                     </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row mx-auto no-gutters panel-content-login-with_social-resources">
        <div class="col-12 align-self-center text-center">
            <div class="panel panel-default enter-with-block">
                <span> <img src="<?=asset('img/line_1.png');?>" alt="" class="d-xl-inline-block d-lg-inline-block d-md-inline-block d-sm-none d-none"> </span>
                <span class="enter-with-text">Войти с помощью</span>
                <span> <img src="<?=asset('img/line_1.png');?>" alt="" class="d-xl-inline-block d-lg-inline-block d-md-inline-block d-sm-none d-none"> </span>
            </div>
        </div>

        <div class="col text-right mr-2 facebook-register-blc">
            <a href="#">
                <img src="<?=asset('img/facebook-register-img-btn.png');?>" alt="">
            </a>
        </div>
        <div class="col text-left ml-2 google-register-blc">
            <a href="#">
                <img src="<?=asset('img/google-register-img-btn.png');?>" alt="">
            </a>
        </div>
    </div>
</div>
@endsection
