@section('header')
<?php
//$lang = 'en'; App::setLocale( $lang );

//dump($nav_menu);
//dump($vars_for_template_view);
//dump($keywords); dump($meta_description); dump($title);
?>
<?php
$current_route = Route::currentRouteName();
$url_current = \Illuminate\Support\Facades\URL::current();
$site_url =  url()->full();
$authUser = Auth::user();
?>
<div class="container">
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col left-part">
            <div class="header-left">
                <div class="logo">
                    <a href="/">
                        <img src="<?=asset('img/icon_7.png');?>" alt=""/>
                        <span>Lapki</span>
                    </a>
                </div>
                <div class="top_info d-none d-xl-inline-block">
                    №1 по поиску случек в Украине!
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col px-xl-0 px-lg-0 px-md-3 px-sm-3 right-part">
            <div class="header-right">

               <?php if( $authUser ): ?>
               <div class="profile profile-pet mr-2">
                    <a href="#" v-on:click="hideShowBlockPet">
                        <img src="<?=asset('img/hatoco_pet_icon.jpg');?>" alt="" class="icon-pet-profile">
                    </a>
               </div>
               <div class="col pet-list-block" v-if="checkHideShowBlockPet">
                    <a href="#" class="pet-item-link" id="pet_item_link_1">
                        <img src="<?=asset('img/hatoco_pet_icon.jpg');?>" alt="" class="icon-pet-profile">
                        <span class="pet-item-name-text d-xl-inline d-lg-inline d-md-inline d-sm-inline d-none">Пушистик</span>
                    </a>
                    <a href="#" class="pet-item-link pet-item-link-active" id="pet_item_link_2">
                        <img src="<?=asset('img/hatoco_pet_icon.jpg');?>" alt="" class="icon-pet-profile">
                        <span class="pet-item-name-text d-xl-inline d-lg-inline d-md-inline d-sm-inline d-none">Зубастик</span>
                    </a>
                    <a href="#" class="pet-item-link" id="pet_item_link_3">
                        <img src="<?=asset('img/hatoco_pet_icon.jpg');?>" alt="" class="icon-pet-profile">
                        <span class="pet-item-name-text d-xl-inline d-lg-inline d-md-inline d-sm-inline d-none">Ушастик</span>
                    </a>
                    <a href="#" class="pet-item-link" id="pet_item_link_4">
                        <img src="<?=asset('img/hatoco_pet_icon.jpg');?>" alt="" class="icon-pet-profile">
                        <span class="pet-item-name-text d-xl-inline d-lg-inline d-md-inline d-sm-inline d-none">Рогастик</span>
                    </a>
                </div>
                <?php endif;?>


                <div class="profile profile-user d-none d-xl-inline-block">
                    <!--Authentication Links-->
                    @guest
                    <a href="{{ route('login') }}">
                        <i class="far fa fa-user"></i>
                        <span class="d-none d-xl-inline-block" style="position:relative;top:-5px">Мой профиль</span>
                    </a>
                    @else
                    <a href="#" v-on:click="hideShowBlockUserNav">
                        <img src="<?=asset('img/homer_simpson.jpeg');?>" alt="" class="icon-user-profile">
                        <span class="d-none d-xl-inline-block">
                            <?=($authUser) ? str_limit($authUser->name, 10) : '';?> <span class="caret-user-profile fa fa-caret-down ml-1" id="caret_user_profile"></span>

                            <div class="logout-custom" v-if="checkHideShowBlockUserNav">
                                 <a href="#" id="id_to_profile_user_link">Перейти в профайл</a> <br/>
                                 <a href="{{ route('logout') }}" id="id_logout_custom_link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">{{ csrf_field() }}</form>
                            </div>
                        </span>
                    </a>
                    @endguest
                    <!--Authentication Links-->
                </div>

                <div class="add_btn">
                    <button type="button" class="btn btn-primary d-none d-xl-inline-block" data-toggle="button" aria-pressed="false"
                            autocomplete="off">
                        <a href="#add"> <span class="fa fa-plus d-none d-xl-inline-block"> </span> Добавить питомца</a>
                    </button>

                    <button type="button" class="d-sm-inline-block d-md-inline-block d-lg-inline-block d-xl-none" data-toggle="button" aria-pressed="false"
                            autocomplete="off">
                        <a href="#"> <span class="fa fa-star"></span> </a>
                    </button>
                    <button type="button" class="d-sm-inline-block d-md-inline-block d-lg-inline-block d-xl-none" data-toggle="button" aria-pressed="false"
                            autocomplete="off">
                        <a href="#"> <span class="fa fa-plus"></span> </a>
                    </button>
                </div>
                <div class="profile d-xl-none">
                    @guest
                    <a href="{{ route('login') }}">
                        <i class="far fa fa-user"></i>
                        <span class="d-none d-xl-inline-block"></span>
                    </a>
                    @else
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="far fa fa-user"></i>
                        <span class="d-none d-xl-inline-block"></span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                    @endguest
                </div>

                <div class="lang d-none d-xl-inline-block ml-1">
                    <div class="dropdown d-none d-xl-inline-block ml-0">
                        <a class="btn btn-secondary dropdown-toggle switch-language" href="#" role="button" id="dropdownMenuLink"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            РУС
                        </a>

                        <div class="dropdown-menu dropdown-menu-list-language" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="#">РУС</a>
                            <a class="dropdown-item" href="#">УКР</a>
                        </div>
                    </div>
                </div>

                {{--<div class="lang d-none d-xl-inline-block">--}}
                    {{--<div class="dropdown d-none d-xl-inline-block">--}}
                        {{--<a class="btn btn-secondary switch-language" href="#" role="button" id="dropdownMenuLink"--}}
                           {{--v-on:click="hideShowMsessage">--}}
                            {{--РУС--}}
                        {{--</a>--}}

                        {{--<div class="dropdown-menu-list-language" v-if="checkShowLangPanel">--}}
                            {{--<a class="dropdown-item" href="#">РУС</a>--}}
                            {{--<a class="dropdown-item" href="#">УКР</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

            </div>
        </div>
    </div>
</div>
@endsection