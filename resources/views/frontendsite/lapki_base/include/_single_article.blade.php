<?php
//dump($right_sidebar_content);
//dump($right_block_popular_content);
//dump($name_method_info);

( $right_sidebar_content ) ? $class_num = 8 : $class_num = 12;
?>

<!--Articles section-->
<section id="single_articles_section" class="articles-section single-articles-section">
    <div class="container">
        <div class="row">
            <div class="col-12 d-xl-inline d-lg-inline d-md-none d-sm-none d-none">
                <nav aria-label="breadcrumb" class="breadcrumb-custom">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route( 'articles' ) }}">Статьи</a></li>
                        <li class="breadcrumb-item"><a href="#">Собаки</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Милые собаки и кошки впервые собрались на выставке в Киеве</li>
                    </ol>
                </nav>
            </div>

            <div class="col-xl-<?=$class_num;?> col-lg-<?=$class_num;?> col-md-12 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2">
                <section id="posts_articles_section" class="home_posts posts-articles-section">
                    <div class="row">
                        <div id="article_list_item_0" class="col-12">
                            <div class="big_thumb mb-4">
                                <div class="img_thumb " style="background-image: url(<?=asset('img/photo_5@2x.png');?>)">
                                </div>
                                <div class="txt">
                                    <div class="post_date_thumb">
                                        19 октября
                                    </div>
                                    <div class="post_thmb_title">
                                        Милые собаки и кошки впервые собрались на выставке в Киеве
                                    </div>
                                    <div class="post_thmb_description">
                                        <p>
                                            СК «Стрела» превратился в место, куда съехались любители собак не только со всей Украины, но и стран зарубежья. Для собаковода здесь хорошее место завести друзей, близких по духу, а для собак — отличный шанс показать себя во всей красе.
                                        </p>
                                        <p>
                                            Участница выставки, Надежда, хозяйка ангорского кота, рассказала, что в подобном мероприятии она принимает участие уже второй раз. Женщина добавила, что разного рода выставки и фестивали — для котов большой стресс. Многие хозяева дают своим питомцам гормональные препараты для успокоения, но сама женщина успокаивает котика вкусным кормом или препаратами на травах.
                                            Другая участница мероприятия, Катя, рассказала, что стать первым и заслужить золото на выставке не так уж просто. Первенство достается лишь тем котам, которые соответствуют строгим правилам.
                                        </p>

                                        <div class="img_thumb d-none d-sm-none d-md-block d-xl-block d-lg-block" style="background-image: url(<?=asset('img/bitmap.png');?>)">
                                        </div>

                                        <p>
                                            На выставке котов взвешивали, проверяли их реакцию на шуршащие игрушки и тискали их пушистые мордочки. Победители получали медали, кубки, лакомства от спонсора, а также удобные «сиделки» и забавные игрушки. Более того, на мероприятии работал детский уголок, где малыши могли порисовать, поиграть и сделать себе кошачий мэйк-ап. В ТЦ «Наша Правда» также можно было приобрести котов, цены варьировались от 600 гривен за котенка до 10 000 гривен за взрослого кота-чемпиона. Ранее мы рассказывали о том, какие животные ищут дом в Днепре.
                                        </p>
                                    </div>
                                    <div class="post_thmb_direction">
                                        <span class="comment-it"> <a href="#" class="btn btn-danger">Комментарий</a> </span>
                                        <span class="like-it social-link"> <span class="d-none d-sm-inline d-md-inline d-xl-inline d-lg-inline">Поделиться:</span>
                                            <a href="#" target="_blank"> <img src="<?=asset('img/facebook-icon-article.png');?>"> </a>
                                            <a href="#" target="_blank"> <img src="<?=asset('img/twitter-icon-article.png');?>"> </a>
                                            <a href="#" target="_blank"> <img src="<?=asset('img/googleplus-icon-article.png');?>"> </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div> <!--/.row-->
                </section> <!--/section.posts-articles-section-->
            </div> <!--/.col-->

            <?php if($right_sidebar_content): ?>
            <div class="col order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1">
                <section id="right_sidebar" class="right-sidebar">
                    <!--Right Side-bar-->
                    <?=$right_sidebar_content;?>
                    <!--Right Side-bar-->
                </section>

                <?php if($right_block_popular_content): ?>
                <section id="popular_block" class="popular_block d-xl-block d-lg-block d-md-none d-sm-none d-none">
                    <!--Popular block-->
                    <?=$right_block_popular_content;?>
                    <!--Popular block-->
                </section>
                <?php endif; ?>
            </div> <!--/.col-->


            <?php if($right_block_popular_content): ?>
            <div class="col d-xl-none d-lg-none d-md-block d-sm-block d-block order-3 popular-block-separately-for-mobile">
                <section id="popular_block" class="popular_block">
                    <!--Popular block-->
                <?=$right_block_popular_content;?>
                <!--Popular block-->
                </section>
            </div>
            <?php endif; ?>

            <?php endif; ?>
        </div> <!--/.row-->
    </div> <!--/.container-->
</section>
<!--/Articles section-->
