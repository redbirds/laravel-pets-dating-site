<?php
//dump($portfolios);
//dump($portfolio_filters);
//dump($right_sidebar_content);

/* Data for Desktop *
    dump($keyword_search);
    dump($city_search);
/
/* Data for Mobile
    dump($keyword_search_mob);
    dump($pettype_search_mob);
    dump($gender_search_mob);
    dump($breed_search_mob);
    dump($city_search_mob);
*/

/*
    if( isset($is_desktop_transfer) ){
        dd($is_desktop_transfer);
    }
    else{
        dd('$is_desktop_transfer NOT exists!!!');
    }
*/
?>
<!-- Home Section -->
<section id="home_filters_search_result" class="home-filters home-filters-search-result">
    <div class="container">
        <div class="row">
        <?php if( $is_desktop_transfer == '_desktop' ):?>
            <!--Desktop version-->
            <div class="col-sm-12 d-none d-lg-block d-xl-block">
                <form class="main_filter" name="main_filter" action="" method="post" novalidate>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="input-group search">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><img src="<?=asset('img/shape_18.png');?>"/></span>
                        </div>
                        <input type="text" name="keyword" class="form-control" placeholder="Поиск по ключевому слову +++++"
                               aria-label="search"
                               aria-describedby="basic-addon1" value="<?=$keyword_search;?>">
                    </div>
                    <div class="input-group city">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon2"><img src="<?=asset('img/icon_5.png');?>"/></span>
                        </div>
                        <input type="text" name="city" class="form-control" placeholder="Город"
                               aria-label="city"
                               aria-describedby="basic-addon1" value="<?=$city_search;?>">
                    </div>
                    <a href="">
                        <button type="submit" class="btn btn-danger">Поиск</button>
                    </a>
                </form>
            </div>
            <!--/Desktop version-->
        <?php elseif( $is_desktop_transfer == '_mobile' ):?>
            <!--Mobile version-->
            <div class="col-sm-12 d-lg-none d-xl-none">
                <h2 class="section_title">Найдите питомца для случки</h2>

                <form class="main_filter main_filter_mobver" name="main_filter">
                    <div class="input-group search">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><img src="<?=asset('img/shape_18.png');?>"/></span>
                        </div>

                        <input type="text" class="form-control" placeholder="Введите ключевые слова"
                               aria-label="search"
                               aria-describedby="basic-addon1">

                        <div class="input-group-append collapsed" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            <span class="input-group-text fa fa-filter" id="basic-addon1"></span>
                            <span class="input-group-text fa" id="basic-addon2"></span>
                        </div>
                    </div>

                    <div class="collapse" id="collapseExample">
                        <div class="card card-body card-filter-content">
                            <label for="pet_type">Тип питомца</label>
                            <div class="select-outer">
                                <select name="pet_type" class="form-control width-auto" id="pet_type">
                                    <option value="dogs">Собака</option>
                                    <option value="cats">Кошка</option>
                                    <option value="rabbits">Кролик</option>
                                    <option value="guinea-pigs">Морская свинка</option>
                                    <option value="ferret">Хорек</option>
                                    <option value="chinchillas">Шиншилла</option>
                                    <option value="birds">Птица</option>
                                    <option value="turtles">Черепаха</option>
                                    <option value="horses">Лошадь</option>
                                </select>
                                <a href="" class="select-outer-btn"></a>
                            </div>

                            <label for="gender">Пол</label>
                            <select name="gender" class="form-control width-auto" id="gender">
                                <option value="1">Кабель</option>
                                <option value="2">Сука</option>
                            </select>

                            <label for="breed">Порода</label>
                            <select name="breed" class="form-control width-auto" id="breed">
                                <option value="breed_1">Порода-1</option>
                                <option value="breed_2">Порода-2</option>
                                <option value="breed_3">Порода-3</option>
                                <option value="breed_4">Порода-4</option>
                            </select>

                            <label for="city">Город</label>
                            <select name="city" class="form-control width-auto" id="city">
                                <option value="dnepr">Днепр</option>
                                <option value="zaporizhye">Запорожье</option>
                                <option value="kyiv">Киев</option>
                                <option value="lviv">Львов</option>
                                <option value="Odesa">Одесса</option>
                                <option value="kharkiv">Харьков</option>
                            </select>
                        </div>
                    </div>
                    <a href="">
                        <button type="submit" class="btn btn-danger">Поиск</button>
                    </a>
                </form>
            </div>
            <!--/Mobile version-->
        <?php else: ?>
            <!--Desktop version without POST Data-->
            <!--Desktop version-->
            <div class="col-sm-12 d-none d-lg-block d-xl-block">
                <form class="main_filter" name="main_filter" action="{{ route('search') }}" method="post" novalidate>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="input-group search">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><img src="<?=asset('img/shape_18.png');?>"/></span>
                        </div>
                        <input type="text" name="keyword" class="form-control" placeholder="Поиск по ключевому слову"
                               aria-label="search"
                               aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group city">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon2"><img src="<?=asset('img/icon_5.png');?>"/></span>
                        </div>
                        <input type="text" name="city" class="form-control" placeholder="Город"
                               aria-label="city"
                               aria-describedby="basic-addon1">
                    </div>
                    <a href="">
                        <button type="submit" class="btn btn-danger">Поиск</button>
                    </a>
                </form>
            </div>
            <!--/Desktop version-->
            <!--Mobile version-->
            <div class="col-sm-12 d-lg-none d-xl-none">
                <h2 class="section_title">Найдите питомца для случки</h2>

                <form class="main_filter main_filter_mobver" name="main_filter" action="{{ route('search') }}" method="post" novalidate>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="input-group search">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><img src="<?=asset('img/shape_18.png');?>"/></span>
                        </div>

                        <input type="text" name="keyword" class="form-control" placeholder="Введите ключевые слова"
                               aria-label="search"
                               aria-describedby="basic-addon1">

                        <div class="input-group-append collapsed" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            <span class="input-group-text fa fa-filter" id="basic-addon1"></span>
                            <span class="input-group-text fa" id="basic-addon2"></span>
                        </div>
                    </div>

                    <div class="collapse" id="collapseExample">
                        <div class="card card-body card-filter-content">
                            <label for="pet_type">Тип питомца</label>
                            <div class="select-outer">
                                <select name="pet_type" class="form-control width-auto" id="pet_type">
                                    <option value="dogs">Собака</option>
                                    <option value="cats">Кошка</option>
                                    <option value="rabbits">Кролик</option>
                                    <option value="guinea-pigs">Морская свинка</option>
                                    <option value="ferret">Хорек</option>
                                    <option value="chinchillas">Шиншилла</option>
                                    <option value="birds">Птица</option>
                                    <option value="turtles">Черепаха</option>
                                    <option value="horses">Лошадь</option>
                                </select>
                                <a href="" class="select-outer-btn"></a>
                            </div>

                            <label for="gender">Пол</label>
                            <select name="gender" class="form-control width-auto" id="gender">
                                <option value="1">Кабель</option>
                                <option value="2">Сука</option>
                            </select>

                            <label for="breed">Порода</label>
                            <select name="breed" class="form-control width-auto" id="breed">
                                <option value="breed_1">Порода-1</option>
                                <option value="breed_2">Порода-2</option>
                                <option value="breed_3">Порода-3</option>
                                <option value="breed_4">Порода-4</option>
                            </select>

                            <label for="city">Город</label>
                            <select name="city" class="form-control width-auto" id="city">
                                <option value="dnepr">Днепр</option>
                                <option value="zaporizhye">Запорожье</option>
                                <option value="kyiv">Киев</option>
                                <option value="lviv">Львов</option>
                                <option value="Odesa">Одесса</option>
                                <option value="kharkiv">Харьков</option>
                            </select>
                        </div>
                    </div>

                    <a href="">
                        <button type="submit" class="btn btn-danger">Поиск</button>
                    </a>
                </form>
            </div>
            <!--/Mobile version-->
            <!--/Desktop version without POST Data-->
        <?php endif;?>
        <!--/Mobile version-->
        </div> <!--/row-->
    </div> <!--/container-->

    <div class="container">
        <div class="row">
            <div class="col-sm-12 d-none d-lg-block d-xl-block"> </div>
        </div>
    </div>
</section>



<section class="animal_section">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 col-lg-9 col-md-9 col-sm-9">
                <div class="section_title wow slideInLeft" data-wow-duration="2.5s" data-wow-delay="0.3s" data-wow-offset="80">
                    <span id="find_it_num" class="find-it-num">189</span> найдено
                </div>
            </div>
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-3 wow slideInUp" data-wow-duration="3.5s" data-wow-delay="0.3s" data-wow-offset="80">
                <div class="link_to_all">
                    <a href="#">
                        <span class="look-span d-none d-md-inline-block d-lg-inline-block d-xl-inline-block">Cмотреть</span> все <img src="<?=asset('img/icon-arrov.png');?>" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="animal_thumb">

                    <div class="single_animal_thumb">
                        <div class="photo_section" style="background-image: url('img/photo_15.png');">
                            <div class="block">
                                <div class="favorite">
                                    <a href="#">
                                        <i class="fa fa-star-o"></i>
                                    </a>
                                </div>
                                <div class="like">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path d="M8.454 0c-.791 0-.525 1.579-.525 1.579s-1.692 4.353-3.31 5.513a3.311 3.311 0 0 0-.785 1.236c-.109.206-.34.523-.834.8l1.618 6.567s2.505.304 5.017.255c1.005.075 2.07.081 2.918-.089 2.88-.572 2.158-2.444 2.158-2.444 1.552-1.089.67-2.448.67-2.448 1.38-1.344.024-2.471.024-2.471s.746-1.085-.218-1.906c-1.201-1.026-4.463-.343-4.463-.343-.227.036-.469.082-.728.14 0 0-1.128.49 0-2.702C11.127.494 9.246 0 8.454 0z"/>
                                            <path d="M4.985 15.4L3.569 9.603C3.488 9.272 3.099 9 2.707 9H.004L0 16h4.414c.397.003.652-.268.571-.6z"/>
                                        </g>
                                    </svg>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="info_section">
                            <div class="name">
                                Жорик
                            </div>
                            <div class="city">
                                <i class="fas fa-map-marker-alt"></i><span>Запорожье</span>
                            </div>
                            <div class="age">
                                <img src="<?=asset('img/shape_6.png');?>"/><span>2 года</span>
                            </div>
                            <div class="breed">
                                <span><img src="<?=asset('img/shape_5.png');?>"/></span><span>Австралийская короткохвостая</span>
                            </div>

                            <div class="price_section">
                                <div class="price">
                                    $101
                                </div>
                                <div class="gender">
                                    <img src="<?=asset('img/shape_4.png');?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single_animal_thumb">
                        <div class="photo_section" style="background-image: url('img/photo_13.png');">
                            <div class="block">
                                <div class="favorite">
                                    <a href="#">
                                        <i class="fa fa-star-o"></i>
                                    </a>
                                </div>
                                <div class="like">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path d="M8.454 0c-.791 0-.525 1.579-.525 1.579s-1.692 4.353-3.31 5.513a3.311 3.311 0 0 0-.785 1.236c-.109.206-.34.523-.834.8l1.618 6.567s2.505.304 5.017.255c1.005.075 2.07.081 2.918-.089 2.88-.572 2.158-2.444 2.158-2.444 1.552-1.089.67-2.448.67-2.448 1.38-1.344.024-2.471.024-2.471s.746-1.085-.218-1.906c-1.201-1.026-4.463-.343-4.463-.343-.227.036-.469.082-.728.14 0 0-1.128.49 0-2.702C11.127.494 9.246 0 8.454 0z"/>
                                            <path d="M4.985 15.4L3.569 9.603C3.488 9.272 3.099 9 2.707 9H.004L0 16h4.414c.397.003.652-.268.571-.6z"/>
                                        </g>
                                    </svg>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="info_section">
                            <div class="name">
                                Вилли
                            </div>
                            <div class="city">
                                <i class="fas fa-map-marker-alt"></i><span>Харьков</span>
                            </div>
                            <div class="age">
                                <img src="<?=asset('img/shape_6.png');?>"/><span>2 года</span>
                            </div>
                            <div class="breed">
                                <span><img src="<?=asset('img/shape_5.png');?>"/></span><span>Австралийская короткохвостая</span>
                            </div>

                            <div class="price_section">
                                <div class="price">
                                    $180
                                </div>
                                <div class="gender">
                                    <img src="<?=asset('img/shape_4.png');?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single_animal_thumb">
                        <div class="photo_section" style="background-image: url('img/photo_12.png');">
                            <div class="block">
                                <div class="favorite">
                                    <a href="#">
                                        <i class="fa fa-star-o"></i>
                                    </a>
                                </div>
                                <div class="like">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path d="M8.454 0c-.791 0-.525 1.579-.525 1.579s-1.692 4.353-3.31 5.513a3.311 3.311 0 0 0-.785 1.236c-.109.206-.34.523-.834.8l1.618 6.567s2.505.304 5.017.255c1.005.075 2.07.081 2.918-.089 2.88-.572 2.158-2.444 2.158-2.444 1.552-1.089.67-2.448.67-2.448 1.38-1.344.024-2.471.024-2.471s.746-1.085-.218-1.906c-1.201-1.026-4.463-.343-4.463-.343-.227.036-.469.082-.728.14 0 0-1.128.49 0-2.702C11.127.494 9.246 0 8.454 0z"/>
                                            <path d="M4.985 15.4L3.569 9.603C3.488 9.272 3.099 9 2.707 9H.004L0 16h4.414c.397.003.652-.268.571-.6z"/>
                                        </g>
                                    </svg>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="info_section">
                            <div class="name">
                                Микеланджело
                            </div>
                            <div class="city">
                                <i class="fas fa-map-marker-alt"></i><span>Днепр</span>
                            </div>
                            <div class="age">
                                <img src="<?=asset('img/shape_6.png');?>"/><span>8 лет</span>
                            </div>
                            <div class="breed">
                                <span><img src="<?=asset('img/shape_3.png');?>"/></span><span>Персидская длиннохвостая</span>
                            </div>

                            <div class="price_section">
                                <div class="price">
                                    $2000
                                </div>
                                <div class="gender">
                                    <img src="<?=asset('img/xmlid-450.png');?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single_animal_thumb">
                        <div class="photo_section" style="background-image: url('img/photo_15.png');">
                            <div class="block">
                                <div class="favorite">
                                    <a href="#">
                                        <i class="fa fa-star-o"></i>
                                    </a>
                                </div>
                                <div class="like">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path d="M8.454 0c-.791 0-.525 1.579-.525 1.579s-1.692 4.353-3.31 5.513a3.311 3.311 0 0 0-.785 1.236c-.109.206-.34.523-.834.8l1.618 6.567s2.505.304 5.017.255c1.005.075 2.07.081 2.918-.089 2.88-.572 2.158-2.444 2.158-2.444 1.552-1.089.67-2.448.67-2.448 1.38-1.344.024-2.471.024-2.471s.746-1.085-.218-1.906c-1.201-1.026-4.463-.343-4.463-.343-.227.036-.469.082-.728.14 0 0-1.128.49 0-2.702C11.127.494 9.246 0 8.454 0z"/>
                                            <path d="M4.985 15.4L3.569 9.603C3.488 9.272 3.099 9 2.707 9H.004L0 16h4.414c.397.003.652-.268.571-.6z"/>
                                        </g>
                                    </svg>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="info_section">
                            <div class="name">
                                Вилли
                            </div>
                            <div class="city">
                                <i class="fas fa-map-marker-alt"></i><span>Запорожье</span>
                            </div>
                            <div class="age">
                                <img src="<?=asset('img/shape_6.png');?>"/><span>2 года</span>
                            </div>
                            <div class="breed">
                                <span><img src="<?=asset('img/shape_5.png');?>"/></span><span>Австралийская короткохвостая</span>
                            </div>

                            <div class="price_section">
                                <div class="price">
                                    $450
                                </div>
                                <div class="gender">
                                    <img src="<?=asset('img/shape_4.png');?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single_animal_thumb">
                        <div class="photo_section" style="background-image: url('img/photo_9.png');">
                            <div class="block">
                                <div class="favorite">
                                    <a href="#">
                                        <i class="fa fa-star-o"></i>
                                    </a>
                                </div>
                                <div class="like">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path d="M8.454 0c-.791 0-.525 1.579-.525 1.579s-1.692 4.353-3.31 5.513a3.311 3.311 0 0 0-.785 1.236c-.109.206-.34.523-.834.8l1.618 6.567s2.505.304 5.017.255c1.005.075 2.07.081 2.918-.089 2.88-.572 2.158-2.444 2.158-2.444 1.552-1.089.67-2.448.67-2.448 1.38-1.344.024-2.471.024-2.471s.746-1.085-.218-1.906c-1.201-1.026-4.463-.343-4.463-.343-.227.036-.469.082-.728.14 0 0-1.128.49 0-2.702C11.127.494 9.246 0 8.454 0z"/>
                                            <path d="M4.985 15.4L3.569 9.603C3.488 9.272 3.099 9 2.707 9H.004L0 16h4.414c.397.003.652-.268.571-.6z"/>
                                        </g>
                                    </svg>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="info_section">
                            <div class="name">
                                Билли-Бонс
                            </div>
                            <div class="city">
                                <i class="fas fa-map-marker-alt"></i><span>Киев</span>
                            </div>
                            <div class="age">
                                <img src="<?=asset('img/shape_6.png');?>"/><span>2 года</span>
                            </div>
                            <div class="breed">
                                <span><img src="<?=asset('img/shape_5.png');?>"/></span><span>Австралийская короткохвостая</span>
                            </div>

                            <div class="price_section">
                                <div class="price">
                                    $180
                                </div>
                                <div class="gender">
                                    <img src="<?=asset('img/shape_4.png');?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single_animal_thumb">
                        <div class="photo_section" style="background-image: url('img/photo_10.png');">
                            <div class="block">
                                <div class="favorite">
                                    <a href="#">
                                        <i class="fa fa-star-o"></i>
                                    </a>
                                </div>
                                <div class="like">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path d="M8.454 0c-.791 0-.525 1.579-.525 1.579s-1.692 4.353-3.31 5.513a3.311 3.311 0 0 0-.785 1.236c-.109.206-.34.523-.834.8l1.618 6.567s2.505.304 5.017.255c1.005.075 2.07.081 2.918-.089 2.88-.572 2.158-2.444 2.158-2.444 1.552-1.089.67-2.448.67-2.448 1.38-1.344.024-2.471.024-2.471s.746-1.085-.218-1.906c-1.201-1.026-4.463-.343-4.463-.343-.227.036-.469.082-.728.14 0 0-1.128.49 0-2.702C11.127.494 9.246 0 8.454 0z"/>
                                            <path d="M4.985 15.4L3.569 9.603C3.488 9.272 3.099 9 2.707 9H.004L0 16h4.414c.397.003.652-.268.571-.6z"/>
                                        </g>
                                    </svg>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="info_section">
                            <div class="name">
                                Шпили Вилли
                            </div>
                            <div class="city">
                                <i class="fas fa-map-marker-alt"></i><span>Запорожье</span>
                            </div>
                            <div class="age">
                                <img src="<?=asset('img/shape_6.png');?>"/><span>2 года</span>
                            </div>
                            <div class="breed">
                                <span><img src="<?=asset('img/shape_5.png');?>"/></span><span>Австралийская короткохвостая</span>
                            </div>

                            <div class="price_section">
                                <div class="price">
                                    $240
                                </div>
                                <div class="gender">
                                    <img src="<?=asset('img/xmlid-450.png');?>"/>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="home_posts">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 col-lg-9 col-md-9 col-sm-9">
                <div class="section_title wow slideInLeft" data-wow-duration="2.0s" data-wow-delay="0.3s" data-wow-offset="80">
                    Последние статьи
                </div>
            </div>
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-3 wow slideInUp" data-wow-duration="3.5s" data-wow-delay="0.3s" data-wow-offset="80">
                <div class="link_to_all">
                    <a href="#">
                        <span class="look-span d-none d-md-inline-block d-lg-inline-block d-xl-inline-block">Cмотреть</span> все <img src="<?=asset('img/icon-arrov.png');?>" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-xl-4 col-lg-12 d-none d-sm-none d-xl-block d-lg-block">
                <div class="big_thumb mb-4">
                    <a href="#">
                        <div class="img_thumb " style="background-image: url('img/photo_5@2x.png')">
                        </div>
                        <div class="txt">
                            <div class="post_date_thumb">
                                19 октября
                            </div>
                            <div class="post_thmb_title">
                                Милые собаки и кошки впервые собрались на выставке в Киеве
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6">
                <div class="small_thumb">
                    <a href="#">
                        <div class="media">
                            <div class="img_thmb"
                                 style="background-image: url('img/photo_4@3x.png')"></div>
                            <div class="media-body">
                                <div class="post_date_thumb">
                                    19 октября
                                </div>
                                <div class="post_thmb_title">
                                    Собаки и осенняя аллергия. Что делать как быть? Что?
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="small_thumb">
                    <a href="#">
                        <div class="media">
                            <div class="img_thmb"
                                 style="background-image: url('img/photo_4@3x.png')"></div>
                            <div class="media-body">
                                <div class="post_date_thumb">
                                    19 октября
                                </div>
                                <div class="post_thmb_title">
                                    Собаки и осенняя аллергия. Что делать как быть? Что?
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6">
                <div class="small_thumb">
                    <a href="#">
                        <div class="media">
                            <div class="img_thmb"
                                 style="background-image: url('img/photo_4@3x.png')"></div>
                            <div class="media-body">
                                <div class="post_date_thumb">
                                    19 октября
                                </div>
                                <div class="post_thmb_title">
                                    Собаки и осенняя аллергия. Что делать как быть? Что?
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="small_thumb">
                    <a href="#">
                        <div class="media">
                            <div class="img_thmb"
                                 style="background-image: url('img/photo_4@3x.png')"></div>
                            <div class="media-body">
                                <div class="post_date_thumb">
                                    19 октября
                                </div>
                                <div class="post_thmb_title">
                                    Собаки и осенняя аллергия. Что делать как быть? Что?
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Home Section -->