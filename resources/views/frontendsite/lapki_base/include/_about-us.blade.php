<?php
//dump($portfolios);
//dump($portfolio_filters);
//dump($right_sidebar_content);
?>

<!--About-Us section-->
<section id="about_us_section" class="about-us-section">
    <div class="container">
        <div class="row">
            <div class="col" style="background: white;">
                <div class="about-us-info-wrap">
                    <div class="row">
                        <div class="col">
                            <h1 class="wow fadeInUp" data-wow-duration="2.5s" data-wow-delay="0.9s" data-wow-offset="80">О нас</h1>
                            <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.3s" data-wow-offset="80">
                                Если для входа в Профиль OLX вы используете вышеуказанные почтовые сервисы или социальные сети, мы настоятельно рекомендуем сменить электронный адрес, который вы используете для входа в Профиль OLX на тот, который не подлежит санкциям. Таким образом, вы сохраните доступ.
                            </p>
                            <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.3s" data-wow-offset="80">
                                Профиль OLX вы используете вышеуказанные почтовые сервисы или социальные сети, мы настоятельно рекомендуем сменить электронный адрес, который вы используете для входа в Профиль OLX на тот, который не подлежит санкциям.
                            </p>
                        </div>

                        <div class="col col-about-us-info-bg d-none d-xl-block d-lg-block">
                            <div class="about-us-info-bg">
                            </div>
                        </div>
                    </div>
                </div> <!--/.about-us-info-wrap-->
            </div> <!--/.col-->
        </div> <!--/.row-->

        <div class="row justify-content-between">
            <div class="col-7 col-md-12 col-xl-7 col-lg-7 col-sm-12 col-12 accordion-custom-grid-wrap">
                <h1 class="wow fadeInUp" data-wow-duration="2.5s" data-wow-delay="0.9s" data-wow-offset="80">Как это работает</h1>
                <div class="accordion accordion-custom" id="accordionExample1">
                    <div class="card">
                        <div class="card-header" id="heading-1">
                            <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false" aria-controls="collapse-1">
                                <span class="accordion-title-custom">User manual</span> <span class="caret-custom fa"></span>
                            </h5>
                        </div>

                        <div id="collapse-1" class="collapse" aria-labelledby="heading-1" data-parent="#accordionExample1">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading-2">
                            <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapse-2" aria-expanded="false" aria-controls="collapse-2">
                                <span class="accordion-title-custom">Guidelines</span> <span class="caret-custom fa"></span>
                            </h5>
                        </div>
                        <div id="collapse-2" class="collapse" aria-labelledby="heading-2" data-parent="#accordionExample1">
                            <div class="card-body">
                               ++ Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading-3">
                            <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapse-3" aria-expanded="false" aria-controls="collapse-3">
                                <span class="accordion-title-custom">Return policy</span> <span class="caret-custom fa"></span>
                            </h5>
                        </div>
                        <div id="collapse-3" class="collapse" aria-labelledby="heading-3" data-parent="#accordionExample1">
                            <div class="card-body">
                                -- Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading-4">
                            <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapse-4" aria-expanded="false" aria-controls="collapse-4">
                                <span class="accordion-title-custom">Feedback options</span> <span class="caret-custom fa"></span>
                            </h5>
                        </div>
                        <div id="collapse-4" class="collapse" aria-labelledby="heading-4" data-parent="#accordionExample1">
                            <div class="card-body">
                                С момента публикации объявление будет отображаться на OLX в течение 30 дней, после чего - переместится во вкладку Неактивные. По истечению 30 дней объявление можно опубликовать повторно с помощью кнопки Активировать, которая доступна напротив объявления после переместится во вкладку Неактивные.
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--/.col-->
            <div class="col-4 col-md-12 col-xl-4 col-lg-4 col-sm-12 col-12 accordion-custom-grid-wrap accordion-custom-grid-wrap-two">
                <div>
                    <h1 class="wow fadeInUp" data-wow-duration="2.5s" data-wow-delay="0.9s" data-wow-offset="80">Все еще есть вопрос?</h1>
                    <h6 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.5s" data-wow-offset="80">Мы готовы помочь! С 8:00 до 21:00</h6>
                    <a href="{{ route( 'contacts' ) }}" class="btn btn-danger about-us-to-contacts-btn wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.5s" data-wow-offset="80">
                        Контакт
                    </a>
                </div>
            </div> <!--/.col-->


            {{--<div>--}}
                {{--<p class="hover_title_example" v-bind:title="message"> Hover me ... </p> <!--v-bind используется в HTML и называется директивой -->--}}
                {{--<p class="one" v-cloak> @{{ message999 }} </p>--}}
            {{--</div>--}}
            {{--<hr/>--}}


        </div> <!--/.row-->
    </div> <!--/.container-->
</section>
<!--/About-Us section-->
