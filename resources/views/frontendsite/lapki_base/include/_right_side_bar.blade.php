<?php
//dump($data_right_sidebar);
?>
<div id="right_side_bar" class="col-12 right-side-bar">
    <!--Desktop version-->
    <form class="articles-filter-form d-xl-block d-lg-block d-md-none d-sm-none d-none" name="articles-filter-form">
        <div class="input-group search">
            <input type="text" class="form-control articles-filter-form-input" placeholder="Ключевые слова"
                   aria-label="search"
                   aria-describedby="basic-addon1">
            <div class="input-group-append">
                <button type="submit" class="input-group-text articles-filter-form-button" id="articles_filter_form_button"><img src="<?=asset('img/shape_18.png');?>"/></button>
            </div>
        </div>
    </form>
    <hr class="d-xl-block d-lg-block d-md-none d-sm-none d-none">
    <div class="animal_filters animal-filters-desktop-var d-xl-block d-lg-block d-md-none d-sm-none d-none">
        <div class="animal" id="dog-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'dogs']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_5@2x.png');?>"/>
                </div>
                <div class="anml">
                    Собаки
                </div>
            </a>
        </div>
        <div class="animal" id="cat-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'cats']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_3@2x.png');?>"/>
                </div>
                <div class="anml">
                    Кошки
                </div>
            </a>
        </div>
        <div class="animal" id="rabbit-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'rabbits']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_15@2x.png');?>"/>
                </div>
                <div class="anml">
                    Кролики
                </div>
            </a>
        </div>
        <div class="animal" id="guineapig-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'guinea-pigs']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_222@2x.png');?>"/>
                </div>
                <div class="anml">
                    Морские свинки
                </div>
            </a>
        </div>
        <div class="animal" id="foumart-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'ferret']) }}">
                <div class="img" style="top:7px;">
                    <img src="<?=asset('img/shape_14@2x.png');?>"/>
                </div>
                <div class="anml">
                    Хорьки
                </div>
            </a>
        </div>
        <div class="animal" id="сhinchilla-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'chinchillas']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_13@2x.png');?>"/>
                </div>
                <div class="anml">
                    Шиншиллы
                </div>
            </a>
        </div>
        <div class="animal" id="aves-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'birds']) }}">
                <div class="img" style="top:7px;">
                    <img src="<?=asset('img/shape_12@2x.png');?>"/>
                </div>
                <div class="anml">
                    Птицы
                </div>
            </a>
        </div>
        <div class="animal" id="turtle-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'turtles']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_11@2x.png');?>"/>
                </div>
                <div class="anml">
                    Черепахи
                </div>
            </a>
        </div>
        <div class="animal" id="horse-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'horses']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_10@2x.png');?>"/>
                </div>
                <div class="anml">
                    Лошади
                </div>
            </a>
        </div>
    </div>
    <!--/Desktop version-->

    <!--Mobile version-->
    <form class="articles-filter-form articles-filter-form-mobver d-xl-none d-lg-none d-md-block d-sm-block d-block" name="articles-filter-form-mobver">
        <div class="input-group search">
            <div class="input-group-prepend">
                <button type="submit" class="input-group-text articles-filter-form-button" id="articles_filter_form_button"><img src="<?=asset('img/shape_18.png');?>"/></button>
            </div>
            <input type="text" class="form-control articles-filter-form-input" placeholder="Ключевые слова"
                   aria-label="search"
                   aria-describedby="basic-addon1">

            <div class="input-group-append collapsed" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
                <span class="input-group-text fa fa-filter" id="basic-addon1"></span>
                <span class="input-group-text fa" id="basic-addon2"></span>
            </div>
        </div>
    </form>
    <hr/>
    <div class="animal_filters animal-filters-mobile-var collapse" id="multiCollapseExample1">
        <div class="animal" id="dog-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'dogs']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_5@2x.png');?>"/>
                </div>
                <div class="anml">
                    Собаки
                </div>
            </a>
        </div>
        <div class="animal" id="cat-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'cats']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_3@2x.png');?>"/>
                </div>
                <div class="anml">
                    Кошки
                </div>
            </a>
        </div>
        <div class="animal" id="rabbit-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'rabbits']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_15@2x.png');?>"/>
                </div>
                <div class="anml">
                    Кролики
                </div>
            </a>
        </div>
        <div class="animal" id="guineapig-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'guinea-pigs']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_222@2x.png');?>"/>
                </div>
                <div class="anml">
                    Морские свинки
                </div>
            </a>
        </div>
        <div class="animal" id="foumart-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'ferret']) }}">
                <div class="img" style="top:7px;">
                    <img src="<?=asset('img/shape_14@2x.png');?>"/>
                </div>
                <div class="anml">
                    Хорьки
                </div>
            </a>
        </div>
        <div class="animal" id="сhinchilla-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'chinchillas']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_13@2x.png');?>"/>
                </div>
                <div class="anml">
                    Шиншиллы
                </div>
            </a>
        </div>
        <div class="animal" id="aves-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'birds']) }}">
                <div class="img" style="top:7px;">
                    <img src="<?=asset('img/shape_12@2x.png');?>"/>
                </div>
                <div class="anml">
                    Птицы
                </div>
            </a>
        </div>
        <div class="animal" id="turtle-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'turtles']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_11@2x.png');?>"/>
                </div>
                <div class="anml">
                    Черепахи
                </div>
            </a>
        </div>
        <div class="animal" id="horse-i-home">
            <a href="{{ route('articles_cat', ['cat' => 'horses']) }}">
                <div class="img">
                    <img src="<?=asset('img/shape_10@2x.png');?>"/>
                </div>
                <div class="anml">
                    Лошади
                </div>
            </a>
        </div>
    </div>
    <!--/Mobile version-->

</div> <!--/#right_side_bar-->