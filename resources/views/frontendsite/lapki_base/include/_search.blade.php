<?php
//dump($portfolios);
//dump($portfolio_filters);
//dump($right_sidebar_content);

//dump($data_of_post);
//dump($data_of_get);
?>
<!-- Home Section -->
<section id="home_filters_search_result" class="home-filters home-filters-search-result">
    <div class="container">
        <div class="row">
            <!--Desktop version-->
            <div class="col-sm-12 d-none d-lg-block d-xl-block">
                <form class="main_filter" name="main_filter" action="{{ route('search') }}" method="post" novalidate>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="row">
                        <div class="col-12" id="keyword_n_city_input_search">
                                <div class="input-group search">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><img src="<?=asset('img/shape_18.png');?>"/></span>
                                    </div>
                                    <input type="text" name="keyword" class="form-control" placeholder="Поиск по ключевому слову"
                                           aria-label="search"
                                           aria-describedby="basic-addon1"
                                           value="<?=($data_of_post && $data_of_post['keyword']) ? $data_of_post['keyword'] : '';?>">
                                </div>

                            <div class="input-group city">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon2"><img src="<?=asset('img/icon_5.png');?>"/></span>
                                </div>
                                <input type="text" name="city" class="form-control" placeholder="Город"
                                       aria-label="city"
                                       aria-describedby="basic-addon1"
                                       value="<?=($data_of_post && $data_of_post['city']) ? $data_of_post['city'] : '';?>">
                            </div>
                        </div>

                        <div class="col-3" id="pet_type_input_search">
                            <select onchange="changeSelectPetType()" name="pet_type" class="form-control width-auto" id="pet_type">
                                <option value="dogs" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'dogs') ? 'selected' : '';?> <?=($data_of_get && $data_of_get == 'dogs') ? 'selected' : '';?>
                                >Собака</option>
                                <option value="cats" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'cats') ? 'selected' : '';?> <?=($data_of_get && $data_of_get == 'cats') ? 'selected' : '';?>
                                >Кошка</option>
                                <option value="rabbits" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'rabbits') ? 'selected' : '';?> <?=($data_of_get && $data_of_get == 'rabbits') ? 'selected' : '';?>
                                >Кролик</option>
                                <option value="guinea-pigs" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'guinea-pigs') ? 'selected' : '';?> <?=($data_of_get && $data_of_get == 'guinea-pigs') ? 'selected' : '';?>
                                >Морская свинка</option>
                                <option value="ferrets" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'ferrets') ? 'selected' : '';?> <?=($data_of_get && $data_of_get == 'ferrets') ? 'selected' : '';?>
                                >Хорек</option>
                                <option value="chinchillas" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'chinchillas') ? 'selected' : '';?> <?=($data_of_get && $data_of_get == 'chinchillas') ? 'selected' : '';?>
                                >Шиншилла</option>
                                <option value="birds" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'birds') ? 'selected' : '';?> <?=($data_of_get && $data_of_get == 'birds') ? 'selected' : '';?>
                                >Птица</option>
                                <option value="turtles" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'turtles') ? 'selected' : '';?> <?=($data_of_get && $data_of_get == 'turtles') ? 'selected' : '';?>
                                >Черепаха</option>
                                <option value="horses" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'horses') ? 'selected' : '';?> <?=($data_of_get && $data_of_get == 'horses') ? 'selected' : '';?>
                                >Лошадь</option>
                            </select>
                        </div>
                        <div class="col" id="breed_input_search">
                            <select name="breed" class="form-control width-auto" id="breed">
                                <option value="breed_1" <?=($data_of_post && isset($data_of_post['breed']) && $data_of_post['breed'] == 'breed_1') ? 'selected' : '';?>>Порода-1</option>
                                <option value="breed_2" <?=($data_of_post && isset($data_of_post['breed']) && $data_of_post['breed'] == 'breed_2') ? 'selected' : '';?>>Порода-2</option>
                                <option value="breed_3" <?=($data_of_post && isset($data_of_post['breed']) && $data_of_post['breed'] == 'breed_3') ? 'selected' : '';?>>Порода-3</option>
                                <option value="breed_4" <?=($data_of_post && isset($data_of_post['breed']) && $data_of_post['breed'] == 'breed_4') ? 'selected' : '';?>>Порода-4</option>
                            </select>
                        </div>
                        <div class="col-2" id="gender_input_search">
                            <select name="gender" class="form-control width-auto" id="gender">
                                <option value="1" <?=($data_of_post && isset($data_of_post['gender']) && $data_of_post['gender'] == '1') ? 'selected' : '';?>>Кабель</option>
                                <option value="2" <?=($data_of_post && isset($data_of_post['gender']) && $data_of_post['gender'] == '2') ? 'selected' : '';?>>Сука</option>
                            </select>
                        </div>
                        <div class="col" id="color_pet_input_search">
                            <select name="color_pet" class="form-control width-auto" id="color_pet">
                                <option value="black" <?=($data_of_post && isset($data_of_post['color_pet']) && $data_of_post['color_pet'] == 'black') ? 'selected' : '';?>>Черный</option>
                                <option value="white" <?=($data_of_post && isset($data_of_post['color_pet']) && $data_of_post['color_pet'] == 'white') ? 'selected' : '';?>>Белый</option>
                                <option value="pepel" <?=($data_of_post && isset($data_of_post['color_pet']) && $data_of_post['color_pet'] == 'pepel') ? 'selected' : '';?>>Пепельный</option>
                                <option value="patna" <?=($data_of_post && isset($data_of_post['color_pet']) && $data_of_post['color_pet'] == 'patna') ? 'selected' : '';?>>Пятнистый</option>
                            </select>
                        </div>
                        <div class="col-xl col-lg-4 col-md-4 col-sm-4 col-4" id="filter_select_input_search">
                            <select name="filter_select" class="form-control width-auto" id="filter_select">
                                <option value="new_in_start" <?=($data_of_post && isset($data_of_post['filter_select']) && $data_of_post['filter_select'] == 'new_in_start') ? 'selected' : '';?>>Сначала новые</option>
                                <option value="new_in_end" <?=($data_of_post && isset($data_of_post['filter_select']) && $data_of_post['filter_select'] == 'new_in_end') ? 'selected' : '';?>>Новые в конце</option>
                            </select>
                        </div>
                    </div> <!--/.row-->

                    <a href="" style="position: absolute; right: 35px;">
                        <button type="submit" class="btn btn-danger">Поиск</button>
                    </a>
                </form>
            </div>
            <!--/Desktop version-->
            <!--Mobile version-->
            <div class="col-sm-12 d-lg-none d-xl-none">
                <h2 class="section_title">Найдите питомца для случки</h2>

                <form class="main_filter main_filter_mobver" name="main_filter" action="{{ route('search') }}" method="post" novalidate>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="input-group search">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><img src="<?=asset('img/shape_18.png');?>"/></span>
                        </div>

                        <input type="text" name="keyword" class="form-control" placeholder="Введите ключевые слова"
                               aria-label="search"
                               aria-describedby="basic-addon1"
                               value="<?=($data_of_post && $data_of_post['keyword']) ? $data_of_post['keyword'] : '';?>">

                        <div class="input-group-append collapsed" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            <span class="input-group-text fa fa-filter" id="basic-addon1"></span>
                            <span class="input-group-text fa" id="basic-addon2"></span>
                        </div>
                    </div>

                    <div class="collapse" id="collapseExample">
                        <div class="card card-body card-filter-content">
                            <label for="pet_type">Тип питомца</label>
                            <div class="select-outer">
                                <select name="pet_type" class="form-control width-auto" id="pet_type">
                                    <option value="dogs" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'dogs') ? 'selected' : '';?>>Собака</option>
                                    <option value="cats" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'cats') ? 'selected' : '';?>>Кошка</option>
                                    <option value="rabbits" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'rabbits') ? 'selected' : '';?>>Кролик</option>
                                    <option value="guinea-pigs" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'guinea-pigs') ? 'selected' : '';?>>Морская свинка</option>
                                    <option value="ferrets" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'ferrets') ? 'selected' : '';?>>Хорек</option>
                                    <option value="chinchillas" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'chinchillas') ? 'selected' : '';?>>Шиншилла</option>
                                    <option value="birds" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'birds') ? 'selected' : '';?>>Птица</option>
                                    <option value="turtles" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'turtles') ? 'selected' : '';?>>Черепаха</option>
                                    <option value="horses" <?=($data_of_post && isset($data_of_post['pet_type']) && $data_of_post['pet_type'] == 'horses') ? 'selected' : '';?>>Лошадь</option>
                                </select>
                                <a href="" class="select-outer-btn"></a>
                            </div>

                            <label for="gender">Пол</label>
                            <select name="gender" class="form-control width-auto" id="gender">
                                <option value="1" <?=($data_of_post && isset($data_of_post['gender']) && $data_of_post['gender'] == '1') ? 'selected' : '';?>>Кабель</option>
                                <option value="2" <?=($data_of_post && isset($data_of_post['gender']) && $data_of_post['gender'] == '2') ? 'selected' : '';?>>Сука</option>
                            </select>

                            <label for="breed">Порода</label>
                            <select name="breed" class="form-control width-auto" id="breed">
                                <option value="breed_1" <?=($data_of_post && isset($data_of_post['breed']) && $data_of_post['breed'] == 'breed_1') ? 'selected' : '';?>>Порода-1</option>
                                <option value="breed_2" <?=($data_of_post && isset($data_of_post['breed']) && $data_of_post['breed'] == 'breed_2') ? 'selected' : '';?>>Порода-2</option>
                                <option value="breed_3" <?=($data_of_post && isset($data_of_post['breed']) && $data_of_post['breed'] == 'breed_3') ? 'selected' : '';?>>Порода-3</option>
                                <option value="breed_4" <?=($data_of_post && isset($data_of_post['breed']) && $data_of_post['breed'] == 'breed_4') ? 'selected' : '';?>>Порода-4</option>
                            </select>

                            <label for="city">Город</label>
                            <select name="city" class="form-control width-auto" id="city">
                                <option value="dnepr" <?=($data_of_post && isset($data_of_post['city']) && $data_of_post['city'] == 'dnepr') ? 'selected' : '';?>>Днепр</option>
                                <option value="zaporizhye" <?=($data_of_post && isset($data_of_post['city']) && $data_of_post['city'] == 'zaporizhye') ? 'selected' : '';?>>Запорожье</option>
                                <option value="kyiv" <?=($data_of_post && isset($data_of_post['city']) && $data_of_post['city'] == 'kyiv') ? 'selected' : '';?>>Киев</option>
                                <option value="lviv" <?=($data_of_post && isset($data_of_post['city']) && $data_of_post['city'] == 'lviv') ? 'selected' : '';?>>Львов</option>
                                <option value="odesa" <?=($data_of_post && isset($data_of_post['city']) && $data_of_post['city'] == 'odesa') ? 'selected' : '';?>>Одесса</option>
                                <option value="kharkiv" <?=($data_of_post && isset($data_of_post['city']) && $data_of_post['city'] == 'kharkiv') ? 'selected' : '';?>>Харьков</option>
                            </select>

                            <label for="color_pet">Окрас</label>
                            <select name="color_pet" class="form-control width-auto" id="color_pet">
                                <option value="black" <?=($data_of_post && isset($data_of_post['color_pet']) && $data_of_post['color_pet'] == 'black') ? 'selected' : '';?>>Черный</option>
                                <option value="white" <?=($data_of_post && isset($data_of_post['color_pet']) && $data_of_post['color_pet'] == 'white') ? 'selected' : '';?>>Белый</option>
                                <option value="pepel" <?=($data_of_post && isset($data_of_post['color_pet']) && $data_of_post['color_pet'] == 'pepel') ? 'selected' : '';?>>Пепельный</option>
                                <option value="patna" <?=($data_of_post && isset($data_of_post['color_pet']) && $data_of_post['color_pet'] == 'patna') ? 'selected' : '';?>>Пятнистый</option>
                            </select>

                            <label for="filter_select">Сортировка</label>
                            <select name="filter_select" class="form-control width-auto" id="filter_select">
                                <option value="new_in_start" <?=($data_of_post && isset($data_of_post['filter_select']) && $data_of_post['filter_select'] == 'new_in_start') ? 'selected' : '';?>>Сначала новые</option>
                                <option value="new_in_end" <?=($data_of_post && isset($data_of_post['filter_select']) && $data_of_post['filter_select'] == 'new_in_end') ? 'selected' : '';?>>Новые в конце</option>
                            </select>
                        </div>
                    </div>

                    <a href="">
                        <button type="submit" class="btn btn-danger">Поиск</button>
                    </a>
                </form>
            </div>
            <!--/Mobile version-->
        </div> <!--/row-->
    </div> <!--/container-->

    <div class="container">
        <div class="row">
            <div class="col-sm-12 d-none d-lg-block d-xl-block"> </div>
        </div>
    </div>
</section>

<section class="animal_section animal-section-in-search-result">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 col-lg-9 col-md-9 col-sm-9">
                <div class="section_title wow slideInLeft" data-wow-duration="2.5s" data-wow-delay="0.3s" data-wow-offset="80">
                    <span id="find_it_num" class="find-it-num">189</span> найдено
                </div>
            </div>
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-3 wow slideInUp" data-wow-duration="3.5s" data-wow-delay="0.3s" data-wow-offset="80">
                <div class="link_to_all">
                    <a href="#">
                        <span class="look-span d-none d-md-inline-block d-lg-inline-block d-xl-inline-block">Cмотреть</span> все <img src="<?=asset('img/icon-arrov.png');?>" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="animal_thumb">

                    <div class="single_animal_thumb">
                        <div class="photo_section" style="background-image: url('img/photo_15.png');">
                            <div class="block">
                                <div class="favorite">
                                    <a href="#">
                                        <i class="fa fa-star-o"></i>
                                    </a>
                                </div>
                                <div class="like">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path d="M8.454 0c-.791 0-.525 1.579-.525 1.579s-1.692 4.353-3.31 5.513a3.311 3.311 0 0 0-.785 1.236c-.109.206-.34.523-.834.8l1.618 6.567s2.505.304 5.017.255c1.005.075 2.07.081 2.918-.089 2.88-.572 2.158-2.444 2.158-2.444 1.552-1.089.67-2.448.67-2.448 1.38-1.344.024-2.471.024-2.471s.746-1.085-.218-1.906c-1.201-1.026-4.463-.343-4.463-.343-.227.036-.469.082-.728.14 0 0-1.128.49 0-2.702C11.127.494 9.246 0 8.454 0z"/>
                                            <path d="M4.985 15.4L3.569 9.603C3.488 9.272 3.099 9 2.707 9H.004L0 16h4.414c.397.003.652-.268.571-.6z"/>
                                        </g>
                                    </svg>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="info_section">
                            <div class="name">
                                Жорик
                            </div>
                            <div class="city">
                                <i class="fas fa-map-marker-alt"></i><span>Запорожье</span>
                            </div>
                            <div class="age">
                                <img src="<?=asset('img/shape_6.png');?>"/><span>2 года</span>
                            </div>
                            <div class="breed">
                                <span><img src="<?=asset('img/shape_5.png');?>"/></span><span>Австралийская короткохвостая</span>
                            </div>

                            <div class="price_section">
                                <div class="price">
                                    $101
                                </div>
                                <div class="gender">
                                    <img src="<?=asset('img/shape_4.png');?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single_animal_thumb">
                        <div class="photo_section" style="background-image: url('img/photo_13.png');">
                            <div class="block">
                                <div class="favorite">
                                    <a href="#">
                                        <i class="fa fa-star-o"></i>
                                    </a>
                                </div>
                                <div class="like">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path d="M8.454 0c-.791 0-.525 1.579-.525 1.579s-1.692 4.353-3.31 5.513a3.311 3.311 0 0 0-.785 1.236c-.109.206-.34.523-.834.8l1.618 6.567s2.505.304 5.017.255c1.005.075 2.07.081 2.918-.089 2.88-.572 2.158-2.444 2.158-2.444 1.552-1.089.67-2.448.67-2.448 1.38-1.344.024-2.471.024-2.471s.746-1.085-.218-1.906c-1.201-1.026-4.463-.343-4.463-.343-.227.036-.469.082-.728.14 0 0-1.128.49 0-2.702C11.127.494 9.246 0 8.454 0z"/>
                                            <path d="M4.985 15.4L3.569 9.603C3.488 9.272 3.099 9 2.707 9H.004L0 16h4.414c.397.003.652-.268.571-.6z"/>
                                        </g>
                                    </svg>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="info_section">
                            <div class="name">
                                Вилли
                            </div>
                            <div class="city">
                                <i class="fas fa-map-marker-alt"></i><span>Харьков</span>
                            </div>
                            <div class="age">
                                <img src="<?=asset('img/shape_6.png');?>"/><span>2 года</span>
                            </div>
                            <div class="breed">
                                <span><img src="<?=asset('img/shape_5.png');?>"/></span><span>Австралийская короткохвостая</span>
                            </div>

                            <div class="price_section">
                                <div class="price">
                                    $180
                                </div>
                                <div class="gender">
                                    <img src="<?=asset('img/shape_4.png');?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single_animal_thumb">
                        <div class="photo_section" style="background-image: url('img/photo_12.png');">
                            <div class="block">
                                <div class="favorite">
                                    <a href="#">
                                        <i class="fa fa-star-o"></i>
                                    </a>
                                </div>
                                <div class="like">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path d="M8.454 0c-.791 0-.525 1.579-.525 1.579s-1.692 4.353-3.31 5.513a3.311 3.311 0 0 0-.785 1.236c-.109.206-.34.523-.834.8l1.618 6.567s2.505.304 5.017.255c1.005.075 2.07.081 2.918-.089 2.88-.572 2.158-2.444 2.158-2.444 1.552-1.089.67-2.448.67-2.448 1.38-1.344.024-2.471.024-2.471s.746-1.085-.218-1.906c-1.201-1.026-4.463-.343-4.463-.343-.227.036-.469.082-.728.14 0 0-1.128.49 0-2.702C11.127.494 9.246 0 8.454 0z"/>
                                            <path d="M4.985 15.4L3.569 9.603C3.488 9.272 3.099 9 2.707 9H.004L0 16h4.414c.397.003.652-.268.571-.6z"/>
                                        </g>
                                    </svg>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="info_section">
                            <div class="name">
                                Микеланджело
                            </div>
                            <div class="city">
                                <i class="fas fa-map-marker-alt"></i><span>Днепр</span>
                            </div>
                            <div class="age">
                                <img src="<?=asset('img/shape_6.png');?>"/><span>8 лет</span>
                            </div>
                            <div class="breed">
                                <span><img src="<?=asset('img/shape_3.png');?>"/></span><span>Персидская длиннохвостая</span>
                            </div>

                            <div class="price_section">
                                <div class="price">
                                    $2000
                                </div>
                                <div class="gender">
                                    <img src="<?=asset('img/xmlid-450.png');?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single_animal_thumb">
                        <div class="photo_section" style="background-image: url('img/photo_15.png');">
                            <div class="block">
                                <div class="favorite">
                                    <a href="#">
                                        <i class="fa fa-star-o"></i>
                                    </a>
                                </div>
                                <div class="like">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path d="M8.454 0c-.791 0-.525 1.579-.525 1.579s-1.692 4.353-3.31 5.513a3.311 3.311 0 0 0-.785 1.236c-.109.206-.34.523-.834.8l1.618 6.567s2.505.304 5.017.255c1.005.075 2.07.081 2.918-.089 2.88-.572 2.158-2.444 2.158-2.444 1.552-1.089.67-2.448.67-2.448 1.38-1.344.024-2.471.024-2.471s.746-1.085-.218-1.906c-1.201-1.026-4.463-.343-4.463-.343-.227.036-.469.082-.728.14 0 0-1.128.49 0-2.702C11.127.494 9.246 0 8.454 0z"/>
                                            <path d="M4.985 15.4L3.569 9.603C3.488 9.272 3.099 9 2.707 9H.004L0 16h4.414c.397.003.652-.268.571-.6z"/>
                                        </g>
                                    </svg>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="info_section">
                            <div class="name">
                                Вилли
                            </div>
                            <div class="city">
                                <i class="fas fa-map-marker-alt"></i><span>Запорожье</span>
                            </div>
                            <div class="age">
                                <img src="<?=asset('img/shape_6.png');?>"/><span>2 года</span>
                            </div>
                            <div class="breed">
                                <span><img src="<?=asset('img/shape_5.png');?>"/></span><span>Австралийская короткохвостая</span>
                            </div>

                            <div class="price_section">
                                <div class="price">
                                    $450
                                </div>
                                <div class="gender">
                                    <img src="<?=asset('img/shape_4.png');?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single_animal_thumb">
                        <div class="photo_section" style="background-image: url('img/photo_9.png');">
                            <div class="block">
                                <div class="favorite">
                                    <a href="#">
                                        <i class="fa fa-star-o"></i>
                                    </a>
                                </div>
                                <div class="like">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path d="M8.454 0c-.791 0-.525 1.579-.525 1.579s-1.692 4.353-3.31 5.513a3.311 3.311 0 0 0-.785 1.236c-.109.206-.34.523-.834.8l1.618 6.567s2.505.304 5.017.255c1.005.075 2.07.081 2.918-.089 2.88-.572 2.158-2.444 2.158-2.444 1.552-1.089.67-2.448.67-2.448 1.38-1.344.024-2.471.024-2.471s.746-1.085-.218-1.906c-1.201-1.026-4.463-.343-4.463-.343-.227.036-.469.082-.728.14 0 0-1.128.49 0-2.702C11.127.494 9.246 0 8.454 0z"/>
                                            <path d="M4.985 15.4L3.569 9.603C3.488 9.272 3.099 9 2.707 9H.004L0 16h4.414c.397.003.652-.268.571-.6z"/>
                                        </g>
                                    </svg>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="info_section">
                            <div class="name">
                                Билли-Бонс
                            </div>
                            <div class="city">
                                <i class="fas fa-map-marker-alt"></i><span>Киев</span>
                            </div>
                            <div class="age">
                                <img src="<?=asset('img/shape_6.png');?>"/><span>2 года</span>
                            </div>
                            <div class="breed">
                                <span><img src="<?=asset('img/shape_5.png');?>"/></span><span>Австралийская короткохвостая</span>
                            </div>

                            <div class="price_section">
                                <div class="price">
                                    $180
                                </div>
                                <div class="gender">
                                    <img src="<?=asset('img/shape_4.png');?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single_animal_thumb">
                        <div class="photo_section" style="background-image: url('img/photo_10.png');">
                            <div class="block">
                                <div class="favorite">
                                    <a href="#">
                                        <i class="fa fa-star-o"></i>
                                    </a>
                                </div>
                                <div class="like">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path d="M8.454 0c-.791 0-.525 1.579-.525 1.579s-1.692 4.353-3.31 5.513a3.311 3.311 0 0 0-.785 1.236c-.109.206-.34.523-.834.8l1.618 6.567s2.505.304 5.017.255c1.005.075 2.07.081 2.918-.089 2.88-.572 2.158-2.444 2.158-2.444 1.552-1.089.67-2.448.67-2.448 1.38-1.344.024-2.471.024-2.471s.746-1.085-.218-1.906c-1.201-1.026-4.463-.343-4.463-.343-.227.036-.469.082-.728.14 0 0-1.128.49 0-2.702C11.127.494 9.246 0 8.454 0z"/>
                                            <path d="M4.985 15.4L3.569 9.603C3.488 9.272 3.099 9 2.707 9H.004L0 16h4.414c.397.003.652-.268.571-.6z"/>
                                        </g>
                                    </svg>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="info_section">
                            <div class="name">
                                Шпили Вилли
                            </div>
                            <div class="city">
                                <i class="fas fa-map-marker-alt"></i><span>Запорожье</span>
                            </div>
                            <div class="age">
                                <img src="<?=asset('img/shape_6.png');?>"/><span>2 года</span>
                            </div>
                            <div class="breed">
                                <span><img src="<?=asset('img/shape_5.png');?>"/></span><span>Австралийская короткохвостая</span>
                            </div>

                            <div class="price_section">
                                <div class="price">
                                    $240
                                </div>
                                <div class="gender">
                                    <img src="<?=asset('img/xmlid-450.png');?>"/>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="pagination-block-wrap">
                    <div class="col-12 d-flex">
                        <div class="col pagination-block-wrap-1">
                            <p>1 of 34</p>
                        </div>
                        <div class="col pagination-block-wrap-2">
                            <a href="#" class="btn btn-danger more-articles-btn">
                                Далее
                            </a>
                        </div>
                    </div>
                </div>

<!--TEST AREA-->
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="display: none">
                    Запустить модальное окно
                </button>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                ...
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
<!--/TEST AREA-->

            </div>
        </div>
    </div>
</section>
<!-- /Home Section -->