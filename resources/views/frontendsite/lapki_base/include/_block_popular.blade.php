<?php
//dump($data_block_popular);
//dump($name_method_info);
?>
<section id="popular_posts_articles_section" class="home_posts popular-posts-articles-section">
    <div class="row">
        <div class="col">
            <h1 class="wow fadeInDown" data-wow-duration="1.5s" data-wow-delay="0.9s" data-wow-offset="80"><?=$title_of_block_popular?></h1>
        </div>
        <div class="col d-xl-none d-lg-none d-md-block d-sm-block d-block">
            <div class="link_to_all text-right">
                <a href="#">
                   все <img src="http://lapki.test/img/icon-arrov.png" alt="">
                </a>
            </div>
        </div>

        <?php if($name_method_info == 'index'):?> <!--If template for "All Articles of Blog"-->
        <div id="popular_post_item_1" class="col-12 popular-post-item">
            <div class="small_thumb">
                <a href="#">
                    <div class="media">
                        <div class="img_thmb align-self-center" style="background-image: url(<?=asset('img/photo_4@3x.png');?>)"></div>
                        <div class="media-body align-self-center">
                            <div class="post_date_thumb">
                                19 октября
                            </div>
                            <div class="post_thmb_title">
                                Собаки и осенняя аллергия. Что делать как быть? Что?
                            </div>
                            <div class="post_thmb_direction">
                                <span class="like-it">8 понравилось</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div id="popular_post_item_2" class="col-12 popular-post-item">
            <div class="small_thumb">
                <a href="#">
                    <div class="media">
                        <div class="img_thmb align-self-center" style="background-image: url(<?=asset('img/photo_4@3x.png');?>)"></div>
                        <div class="media-body align-self-center">
                            <div class="post_date_thumb">
                                19 октября
                            </div>
                            <div class="post_thmb_title">
                                Собаки и осенняя аллергия. Что делать как быть? Что?
                            </div>
                            <div class="post_thmb_direction">
                                <span class="like-it">8 понравилось</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div id="popular_post_item_2" class="col-12 popular-post-item">
            <div class="small_thumb">
                <a href="#">
                    <div class="media">
                        <div class="img_thmb align-self-center" style="background-image: url(<?=asset('img/photo_4@3x.png');?>)"></div>
                        <div class="media-body align-self-center">
                            <div class="post_date_thumb">
                                19 октября
                            </div>
                            <div class="post_thmb_title">
                                Собаки и осенняя аллергия. Что делать как быть? Что?
                            </div>
                            <div class="post_thmb_direction">
                                <span class="like-it">8 понравилось</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div id="popular_post_item_2" class="col-12 popular-post-item">
            <div class="small_thumb">
                <a href="#">
                    <div class="media">
                        <div class="img_thmb align-self-center" style="background-image: url(<?=asset('img/photo_4@3x.png');?>)"></div>
                        <div class="media-body align-self-center">
                            <div class="post_date_thumb">
                                19 октября
                            </div>
                            <div class="post_thmb_title">
                                Собаки и осенняя аллергия. Что делать как быть? Что?
                            </div>
                            <div class="post_thmb_direction">
                                <span class="like-it">8 понравилось</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <?php elseif($name_method_info == 'show'):?> <!--If template for "Single Article of Blog"-->
        <div id="popular_post_item_1" class="col-12 popular-post-item">
            <div class="small_thumb">
                <a href="#">
                    <div class="media">
                        <div class="img_thmb align-self-center" style="background-image: url(<?=asset('img/photo_4@3x.png');?>)"></div>
                        <div class="media-body align-self-center">
                            <div class="post_date_thumb">
                                19 октября
                            </div>
                            <div class="post_thmb_title">
                                Собаки и осенняя аллергия. Что делать как быть? Что?
                            </div>
                            <div class="post_thmb_direction">
                                <span class="like-it">8 понравилось</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div id="popular_post_item_2" class="col-12 popular-post-item">
            <div class="small_thumb">
                <a href="#">
                    <div class="media">
                        <div class="img_thmb align-self-center" style="background-image: url(<?=asset('img/photo_4@3x.png');?>)"></div>
                        <div class="media-body align-self-center">
                            <div class="post_date_thumb">
                                19 октября
                            </div>
                            <div class="post_thmb_title">
                                Собаки и осенняя аллергия. Что делать как быть? Что?
                            </div>
                            <div class="post_thmb_direction">
                                <span class="like-it">8 понравилось</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div id="popular_post_item_2" class="col-12 popular-post-item">
            <div class="small_thumb">
                <a href="#">
                    <div class="media">
                        <div class="img_thmb align-self-center" style="background-image: url(<?=asset('img/photo_4@3x.png');?>)"></div>
                        <div class="media-body align-self-center">
                            <div class="post_date_thumb">
                                19 октября
                            </div>
                            <div class="post_thmb_title">
                                Собаки и осенняя аллергия. Что делать как быть? Что?
                            </div>
                            <div class="post_thmb_direction">
                                <span class="like-it">8 понравилось</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    <?php endif;?>
    </div> <!--/.row-->
</section>