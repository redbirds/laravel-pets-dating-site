<?php
//dump($right_sidebar_content);
//dump($right_block_popular_content);
//dump($name_method_info);
//dump($article_cat);

( $right_sidebar_content ) ? $class_num = 8 : $class_num = 12;
?>

<!--Articles section-->
<section id="articles_section" class="articles-section">
    <div class="container">
        <div class="row">
            <div class="col-12 <?=($article_cat) ? 'd-xl-inline d-lg-inline d-md-none d-sm-none d-none' : 'd-none';?>">
                <nav aria-label="breadcrumb" class="breadcrumb-custom">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route( 'articles' ) }}">Статьи</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?=$article_cat;?></li>
                    </ol>
                </nav>
            </div>

            <div class="col-12">
                <h1 class="wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.9s" data-wow-offset="80">
                    <?=($article_cat)?"Статьи о $article_cat":'Последние статьи';?>
                </h1>
            </div>

            <div class="col-xl-<?=$class_num;?> col-lg-<?=$class_num;?> col-md-12 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2">
                <section id="posts_articles_section" class="home_posts posts-articles-section">
                        <div class="row">
                            <div id="article_list_item_0" class="col-12">
                                <div class="big_thumb mb-4">
                                    <div class="img_thumb " style="background-image: url(<?=asset('img/photo_5@2x.png');?>)">
                                    </div>
                                    <div class="txt d-xl-block d-lg-block d-md-block d-sm-none d-none">
                                        <div class="post_date_thumb">
                                            19 октября
                                        </div>
                                        <div class="post_thmb_title">
                                            Милые собаки и кошки впервые собрались на выставке в Киеве
                                        </div>
                                        <div class="post_thmb_description d-none d-sm-none d-md-block d-xl-block d-lg-block">
                                            СК «Стрела» превратился в место, куда съехались любители собак не только со всей Украины, но и стран зарубежья. Для собаковода здесь хорошее место завести друзей, близких по духу, а для собак — отличный шанс показать себя во всей красе.
                                        </div>
                                        <div class="post_thmb_direction">
                                            <span class="read-more"> <a href="{{ route('single_articles', ['id' => 22]) }}">Читать далее</a> </span>
                                            <span class="like-it">8 понравилось</span>
                                        </div>
                                    </div>
                                    <div class="txt txt-mobversion d-xl-none d-lg-none d-md-none d-sm-block d-block">
                                        <a href="{{ route('single_articles', ['id' => 22]) }}" class="link-to-single-article-if-mobile">
                                            <div class="post_date_thumb">
                                                19 октября
                                            </div>
                                            <div class="post_thmb_title">
                                                Милые собаки и кошки впервые собрались на выставке в Киеве
                                            </div>
                                            <div class="post_thmb_description d-none d-sm-none d-md-block d-xl-block d-lg-block">
                                                СК «Стрела» превратился в место, куда съехались любители собак не только со всей Украины, но и стран зарубежья. Для собаковода здесь хорошее место завести друзей, близких по духу, а для собак — отличный шанс показать себя во всей красе.
                                            </div>
                                            <div class="post_thmb_direction">
                                                <span class="like-it">8 понравилось</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div id="article_list_item_1" class="col-12">
                                <div class="small_thumb">
                                    <div class="media">
                                        <div class="img_thmb align-self-center" style="background-image: url(<?=asset('img/photo_4@3x.png');?>)"></div>
                                        <div class="media-body align-self-center d-xl-block d-lg-block d-md-block d-sm-none d-none">
                                            <div class="post_date_thumb">
                                                19 октября
                                            </div>
                                            <div class="post_thmb_title">
                                                Собаки и осенняя аллергия. Что делать как быть? Что?
                                            </div>
                                            <div class="post_thmb_description d-none d-sm-none d-md-block d-xl-block d-lg-block">
                                                СК «Стрела» превратился в место, куда съехались любители собак не только со всей Украины, но и стран зарубежья. Для собаковода здесь хорошее место завести друзей, близких по духу, а для собак — отличный шанс показать себя во всей красе.
                                            </div>
                                            <div class="post_thmb_direction">
                                                <span class="read-more"> <a href="#">Читать далее</a> </span>
                                                <span class="like-it">8 понравилось</span>
                                            </div>
                                        </div>

                                        <div class="media-body media-body-mobversion align-self-center d-xl-none d-lg-none d-md-none d-sm-block d-block">
                                            <a href="#" class="link-to-single-article-if-mobile">
                                                <div class="post_date_thumb">
                                                    19 октября
                                                </div>
                                                <div class="post_thmb_title">
                                                    Собаки и осенняя аллергия. Что делать как быть? Что?
                                                </div>
                                                <div class="post_thmb_description d-none d-sm-none d-md-block d-xl-block d-lg-block">
                                                    СК «Стрела» превратился в место, куда съехались любители собак не только со всей Украины, но и стран зарубежья. Для собаковода здесь хорошее место завести друзей, близких по духу, а для собак — отличный шанс показать себя во всей красе.
                                                </div>
                                                <div class="post_thmb_direction">
                                                    <span class="like-it">8 понравилось</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="article_list_item_2" class="col-12">
                                <div class="small_thumb">
                                    <div class="media">
                                        <div class="img_thmb align-self-center" style="background-image: url(<?=asset('img/photo_4@3x.png');?>)"></div>
                                        <div class="media-body align-self-center d-xl-block d-lg-block d-md-block d-sm-none d-none">
                                            <div class="post_date_thumb">
                                                19 октября
                                            </div>
                                            <div class="post_thmb_title">
                                                Собаки и осенняя аллергия. Что делать как быть? Что?
                                            </div>
                                            <div class="post_thmb_description d-none d-sm-none d-md-block d-xl-block d-lg-block">
                                                СК «Стрела» превратился в место, куда съехались любители собак не только со всей Украины, но и стран зарубежья. Для собаковода здесь хорошее место завести друзей, близких по духу, а для собак — отличный шанс показать себя во всей красе.
                                            </div>
                                            <div class="post_thmb_direction">
                                                <span class="read-more"> <a href="#">Читать далее</a> </span>
                                                <span class="like-it">8 понравилось</span>
                                            </div>
                                        </div>

                                        <div class="media-body media-body-mobversion align-self-center d-xl-none d-lg-none d-md-none d-sm-block d-block">
                                            <a href="#" class="link-to-single-article-if-mobile">
                                                <div class="post_date_thumb">
                                                    19 октября
                                                </div>
                                                <div class="post_thmb_title">
                                                    Собаки и осенняя аллергия. Что делать как быть? Что?
                                                </div>
                                                <div class="post_thmb_description d-none d-sm-none d-md-block d-xl-block d-lg-block">
                                                    СК «Стрела» превратился в место, куда съехались любители собак не только со всей Украины, но и стран зарубежья. Для собаковода здесь хорошее место завести друзей, близких по духу, а для собак — отличный шанс показать себя во всей красе.
                                                </div>
                                                <div class="post_thmb_direction">
                                                    <span class="like-it">8 понравилось</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="article_list_item_3" class="col-12">
                                <div class="small_thumb">
                                    <div class="media">
                                        <div class="img_thmb align-self-center" style="background-image: url(<?=asset('img/photo_4@3x.png');?>)"></div>
                                        <div class="media-body align-self-center d-xl-block d-lg-block d-md-block d-sm-none d-none">
                                            <div class="post_date_thumb">
                                                19 октября
                                            </div>
                                            <div class="post_thmb_title">
                                                Собаки и осенняя аллергия. Что делать как быть? Что?
                                            </div>
                                            <div class="post_thmb_description d-none d-sm-none d-md-block d-xl-block d-lg-block">
                                                СК «Стрела» превратился в место, куда съехались любители собак не только со всей Украины, но и стран зарубежья. Для собаковода здесь хорошее место завести друзей, близких по духу, а для собак — отличный шанс показать себя во всей красе.
                                            </div>
                                            <div class="post_thmb_direction">
                                                <span class="read-more"> <a href="#">Читать далее</a> </span>
                                                <span class="like-it">8 понравилось</span>
                                            </div>
                                        </div>

                                        <div class="media-body media-body-mobversion align-self-center d-xl-none d-lg-none d-md-none d-sm-block d-block">
                                            <a href="#" class="link-to-single-article-if-mobile">
                                                <div class="post_date_thumb">
                                                    19 октября
                                                </div>
                                                <div class="post_thmb_title">
                                                    Собаки и осенняя аллергия. Что делать как быть? Что?
                                                </div>
                                                <div class="post_thmb_description d-none d-sm-none d-md-block d-xl-block d-lg-block">
                                                    СК «Стрела» превратился в место, куда съехались любители собак не только со всей Украины, но и стран зарубежья. Для собаковода здесь хорошее место завести друзей, близких по духу, а для собак — отличный шанс показать себя во всей красе.
                                                </div>
                                                <div class="post_thmb_direction">
                                                    <span class="like-it">8 понравилось</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="article_list_item_4" class="col-12">
                                <div class="small_thumb">
                                    <div class="media">
                                        <div class="img_thmb align-self-center" style="background-image: url(<?=asset('img/photo_4@3x.png');?>)"></div>
                                        <div class="media-body align-self-center d-xl-block d-lg-block d-md-block d-sm-none d-none">
                                            <div class="post_date_thumb">
                                                19 октября
                                            </div>
                                            <div class="post_thmb_title">
                                                Собаки и осенняя аллергия. Что делать как быть? Что?
                                            </div>
                                            <div class="post_thmb_description d-none d-sm-none d-md-block d-xl-block d-lg-block">
                                                СК «Стрела» превратился в место, куда съехались любители собак не только со всей Украины, но и стран зарубежья. Для собаковода здесь хорошее место завести друзей, близких по духу, а для собак — отличный шанс показать себя во всей красе.
                                            </div>
                                            <div class="post_thmb_direction">
                                                <span class="read-more"> <a href="#">Читать далее</a> </span>
                                                <span class="like-it">8 понравилось</span>
                                            </div>
                                        </div>

                                        <div class="media-body media-body-mobversion align-self-center d-xl-none d-lg-none d-md-none d-sm-block d-block">
                                            <a href="#" class="link-to-single-article-if-mobile">
                                                <div class="post_date_thumb">
                                                    19 октября
                                                </div>
                                                <div class="post_thmb_title">
                                                    Собаки и осенняя аллергия. Что делать как быть? Что?
                                                </div>
                                                <div class="post_thmb_description d-none d-sm-none d-md-block d-xl-block d-lg-block">
                                                    СК «Стрела» превратился в место, куда съехались любители собак не только со всей Украины, но и стран зарубежья. Для собаковода здесь хорошее место завести друзей, близких по духу, а для собак — отличный шанс показать себя во всей красе.
                                                </div>
                                                <div class="post_thmb_direction">
                                                    <span class="like-it">8 понравилось</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="article_list_item_5" class="col-12">
                                <div class="small_thumb">
                                    <div class="media">
                                        <div class="img_thmb align-self-center" style="background-image: url(<?=asset('img/photo_4@3x.png');?>)"></div>
                                        <div class="media-body align-self-center d-xl-block d-lg-block d-md-block d-sm-none d-none">
                                            <div class="post_date_thumb">
                                                19 октября
                                            </div>
                                            <div class="post_thmb_title">
                                                Собаки и осенняя аллергия. Что делать как быть? Что?
                                            </div>
                                            <div class="post_thmb_description d-none d-sm-none d-md-block d-xl-block d-lg-block">
                                                СК «Стрела» превратился в место, куда съехались любители собак не только со всей Украины, но и стран зарубежья. Для собаковода здесь хорошее место завести друзей, близких по духу, а для собак — отличный шанс показать себя во всей красе.
                                            </div>
                                            <div class="post_thmb_direction">
                                                <span class="read-more"> <a href="#">Читать далее</a> </span>
                                                <span class="like-it">8 понравилось</span>
                                            </div>
                                        </div>

                                        <div class="media-body media-body-mobversion align-self-center d-xl-none d-lg-none d-md-none d-sm-block d-block">
                                            <a href="#" class="link-to-single-article-if-mobile">
                                                <div class="post_date_thumb">
                                                    19 октября
                                                </div>
                                                <div class="post_thmb_title">
                                                    Собаки и осенняя аллергия. Что делать как быть? Что? ++
                                                </div>
                                                <div class="post_thmb_description d-none d-sm-none d-md-block d-xl-block d-lg-block">
                                                    СК «Стрела» превратился в место, куда съехались любители собак не только со всей Украины, но и стран зарубежья. Для собаковода здесь хорошее место завести друзей, близких по духу, а для собак — отличный шанс показать себя во всей красе.
                                                </div>
                                                <div class="post_thmb_direction">
                                                    <span class="like-it">8 понравилось</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pagination-block-wrap">
                                <div class="col-12 d-flex">
                                    <div class="col pagination-block-wrap-1">
                                        <p>1 of 34</p>
                                    </div>
                                    <div class="col pagination-block-wrap-2">
                                        <a href="#" class="btn btn-danger more-articles-btn">
                                            Далее
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div> <!--/.row-->
                </section> <!--/section.posts-articles-section-->
            </div> <!--/.col-->

            <?php if($right_sidebar_content): ?>
            <div class="col order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1">
                <section id="right_sidebar" class="right-sidebar">
                    <!--Right Side-bar-->
                    <?=$right_sidebar_content;?>
                    <!--Right Side-bar-->
                </section>

                <?php if($right_block_popular_content): ?>
                <section id="popular_block" class="popular_block d-xl-block d-lg-block d-lg-md-none d-lg-sm-none d-none">
                    <!--Popular block-->
                <?=$right_block_popular_content;?>
                <!--Popular block-->
                </section>
                <?php endif; ?>
            </div> <!--/.col-->
            <?php endif; ?>
        </div> <!--/.row-->
    </div> <!--/.container-->
</section>
<!--/Articles section-->