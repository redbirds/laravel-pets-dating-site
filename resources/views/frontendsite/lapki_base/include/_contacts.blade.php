<?php
//dump($portfolios);
//dump($portfolio_filters);
//dump($right_sidebar_content);
?>

<!--About-Us section-->
<section id="contacts_section" class="contacts_section">
    <div class="container">
        <div class="row">
            <div class="col" style="background: white;box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);">
                <div class="contact-info-wrap">
                    <div class="row">
                        <div class="col">
                            <h1 class="wow fadeInUp" data-wow-duration="2.5s" data-wow-delay="0.9s" data-wow-offset="80">Контакт</h1>
                            <p class="d-none d-xl-block d-lg-block wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.3s" data-wow-offset="80">
                                Скажи привет нашей дружной команде! Мы всегда рады сообщениям от наших пользователей и стараемся отвечать на них как можно скорее.
                            </p>
                            <p class="d-xl-none d-lg-none d-sm-block d-block">
                                Нужна помощь? Мы готовы помочь!
                            </p>

                            <form class="form-horizontal" name="contact_us_home_form" action="{{ route('contacts') }}" method="post" novalidate>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <div class="col wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.3s" data-wow-offset="80">
                                        <input type="text" class="form-control" name="your_name" id="your_name" placeholder="Ваше имя" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.3s" data-wow-offset="80">
                                        <input type="email" class="form-control" name="your_email" id="your_email" placeholder="E-mail" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.3s" data-wow-offset="80">
                                        <textarea class="form-control" rows="5" name="your_comment" id="your_comment" placeholder="Сообщение"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col wow fadeInUp text-center" data-wow-duration="1.5s" data-wow-delay="0.3s" data-wow-offset="80">
                                        <button type="submit" class="btn btn-lg btn-danger" name="btn_submit_contact_us_home_form" id="btn_submit_contact_us_home_form">Отправить</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col col-contacts-info-bg d-none d-xl-block d-lg-block">
                        </div>
                    </div>
                </div> <!--/.about-us-info-wrap-->


                {{--<div>--}}
                    {{--<mark> <b>#app2:</b> <b>v-bind </b> используется в HTML и называется директивой.Тут с пом.нее добавляется атрибут`title` и ему присваивается з-е из `message` </mark>--}}
                    {{--<p class="hover_title_example" v-bind:title="message"> Hover me ... </p> <!--v-bind используется в HTML и называется директивой -->--}}
                    {{--<p class="one" v-cloak> @{{ message2 }} </p>--}}
                {{--</div>--}}
                {{--<hr/>--}}

                {{--<button class="btn btn-default"--}}
                        {{--v-on:click="checkShow = !checkShow"--}}
                        {{--v-bind:title="(checkShow) ? 'Hide element under button' : 'Show element under button'"--}}
                        {{--v-bind:disabled="false"> Click Me and element with `Hello, Vasua` hide!--}}
                {{--</button>--}}
                {{--<transition name="slide" mode="out-in">--}}
                    {{--<h2 class="title-h2" v-show="checkShow" v-cloak>Hello, Vasua `<b>v-show</b>`</h2>--}}
                {{--</transition>--}}

                {{--<br>--}}

                {{--<button v-on:click="show = !show">--}}
                    {{--Переключить--}}
                {{--</button>--}}
                {{--<transition name="fade">--}}
                    {{--<p v-if="show">привет</p>--}}
                {{--</transition>--}}


            </div> <!--/.col-->
        </div> <!--/.row-->
    </div> <!--/.container-->
</section>
<!--/contacts_section section-->

<style scoped>
    h2.title-h2 { padding:20px; background:lightgreen; }
    .slide-enter{ opacity: 0; }
    .slide-enter-active { transition: opacity 1.2s !important; }
    .slide-leave-active { transition: opacity 0.8s !important; }
    .slide-leave-to { opacity: 0; }


    .fade-enter-active, .fade-leave-active { transition: opacity 1.5s; }
    .fade-enter, .fade-leave-to /* .fade-leave-active до версии 2.1.8 */ { opacity: 0; }
</style>