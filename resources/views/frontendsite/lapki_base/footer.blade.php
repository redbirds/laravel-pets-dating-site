@section('footer')
<?php
//dump($vars_for_template_view);
//dump($keywords); dump($meta_description); dump($title);
?>
<div class="container">
    <div class="row">
        <div class="col-12 col-lg-10">
            <nav class="footer-nav navbar">
                <ul class="navbar-nav navbar flex-row">
                    <li class="nav-item">
                        <a href="{{ route( 'about_us' ) }}" class="">О Нас</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route( 'contacts' ) }}" class="">Контакт</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="">Релкама</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route( 'articles' ) }}" class="">Блог</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="col-12 col-lg-10 text-center d-xl-none d-lg-none">
            <div class="lang-footer-mobver">
                <a href="#" class="active-lang-class">РУС</a>
                <a href="#" class="">УКР</a>
            </div>
        </div>
        <div class="col-12 col-lg-2">
            <div class="socials">
                <a href="#" target="_blank">
                    <img src="<?=asset('img/fb.png');?>">
                </a>
                <a href="#" target="_blank">
                    <img src="<?=asset('img/twt.png');?>">
                </a>
                <a href="#" target="_blank">
                    <img src="<?=asset('img/inst.png');?>">
                </a>
            </div>
        </div>
    </div>
</div>
@endsection


