@extends('layouts.main_layout_site_lapki')

@section('header')
    @include('frontendsite.'.env('THEME').'.header')
@endsection

@section('content')
    <?=$vars_for_template_view['page_content'];?>
@endsection

@section('footer')
    @include('frontendsite.'.env('THEME').'.footer')
@endsection