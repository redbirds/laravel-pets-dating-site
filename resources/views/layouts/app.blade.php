<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="{{ (isset($meta_description)) ? $meta_description : '' }}">
<meta name="keywords" content="{{ (isset($keywords)) ? $keywords : '' }}">
<meta name="author" content="Lutskyi Yaroslav">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="icon" href="<?=asset('favicon/favicon.ico');?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?=asset('favicon/apple-touch-icon.png');?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?=asset('favicon/favicon-32x32.png');?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?=asset('favicon/favicon-16x16.png');?>">
<link rel="manifest" href="<?=asset('favicon/site.webmanifest');?>">
<link rel="mask-icon" href="<?=asset('favicon/safari-pinned-tab.svg');?>" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

<title>{{ (isset($title)) ? $title : 'Site' }}</title> <!--вместо php-тегов в шаблонах `blade` используем удвоенные фигурные скобки -->

{{--<script src="../../assets/js/ie-emulation-modes-warning.js"></script>--}}

<link href="https://fonts.googleapis.com/css?family=Lato:400,500,600,900" rel="stylesheet" type="text/css">

<link href="<?=asset('form-styler-plugin/dist/jquery.formstyler.css');?>" rel="stylesheet" />
<link href="<?=asset('form-styler-plugin/dist/jquery.formstyler.theme.css');?>" rel="stylesheet" />

<!--boot locale CSS-files wow-animation -->
<link href="<?=asset('wow-animation/animate.min.css');?>" rel="stylesheet" media="screen">

<!-- boot locale CSS-files of Bootstrap -->
<link href="<?=asset('bootstrap/css/bootstrap.css');?>" rel="stylesheet" media="screen">
<link href="<?=asset('bootstrap/css/bootstrap-grid.css');?>" rel="stylesheet" media="screen">
<link href="<?=asset('bootstrap/css/bootstrap-reboot.css');?>" rel="stylesheet" media="screen">
<!-- boot font-awesome Icons -->
<link href="<?=asset('css/font-awesome.css');?>" rel="stylesheet">
<!-- boot my custom main CSS-file -->
<link href="<?=asset('css/app.css');?>" rel="stylesheet" type="text/css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?=asset('form-styler-plugin/dist/jquery.formstyler.js');?>" type="text/javascript"></script>
<script>
    (function($) {
        $(function() {
            $('input, select').styler({ selectSearch: true, });
        });
    })(jQuery);
</script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<style>
    section.home-filters { background: url(<?=asset('img/photobcg.png');?>) }
    section.home-filters.home-filters-search-result{ background: none !important;) }
    #footer nav::after { content: url(<?=asset('img/pets.png');?>) }
    input::-webkit-input-placeholder,textarea::-webkit-input-placeholder{color: #d5c2c2 !important;}
    input:-moz-placeholder,textarea:-moz-placeholder{color:#d5c2c2 !important;} /* Firefox 18- */
    input::-moz-placeholder,textarea::-moz-placeholder{color:#d5c2c2 !important;} /* Firefox 19+ */
    input:-ms-input-placeholder,textarea:-ms-input-placeholder{color:#d5c2c2!important;}
    section.about-us-section .about-us-info-wrap .col-about-us-info-bg {background: url(<?=asset('img/about-us-976@402.png');?>);}
    section.contacts_section .contact-info-wrap .col-contacts-info-bg {background: url(<?=asset('img/contact-page-photo.png');?>);}

    {{--section.home-filters-search-result #pet_type_input_search #pet_type-styler:before { background:url(<?=asset('img/shape_5@2x.png');?>); }--}}
        section.home-filters-search-result #pet_type_input_search.dogs #pet_type-styler:before { background:url(<?=asset('img/shape_5@2x.png');?>); }
    section.home-filters-search-result #pet_type_input_search.cats #pet_type-styler:before { background:url(<?=asset('img/shape_3@2x.png');?>); }
    section.home-filters-search-result #pet_type_input_search.rabbits #pet_type-styler:before { background:url(<?=asset('img/shape_15@2x.png');?>); }
    section.home-filters-search-result #pet_type_input_search.guinea-pigs #pet_type-styler:before { background:url(<?=asset('img/shape_222@2x.png');?>); }
    section.home-filters-search-result #pet_type_input_search.ferrets #pet_type-styler:before { background:url(<?=asset('img/shape_14@2x.png');?>); }
    section.home-filters-search-result #pet_type_input_search.chinchillas #pet_type-styler:before { background:url(<?=asset('img/shape_13@2x.png');?>); }
    section.home-filters-search-result #pet_type_input_search.birds #pet_type-styler:before { background:url(<?=asset('img/shape_12@2x.png');?>); }
    section.home-filters-search-result #pet_type_input_search.turtles #pet_type-styler:before { background:url(<?=asset('img/shape_11@2x.png');?>); }
    section.home-filters-search-result #pet_type_input_search.horses #pet_type-styler:before { background:url(<?=asset('img/shape_10@2x.png');?>); }

    @media screen and (max-width: 399px) {
        .home-filters .main_filter .search input {font-size: 14px;}
    }
</style>
</head>
<body>

<?php
//$lang = 'en'; App::setLocale( $lang );

//dump($nav_menu);
//dump($vars_for_template_view);
//dump($keywords); dump($meta_description); dump($title);
?>
<?php
$current_route = Route::currentRouteName();
$url_current = \Illuminate\Support\Facades\URL::current();
$url_current_slash = $url_current.'/';
$site_url =  url()->full();
?>
<section id="header" class="header header-not-home header-for-auth-template">
    <div class="container">
        <div class="row">
            <div class="col-6 col-md-6 col-sm-6">
                <div class="header-left">
                    <div class="logo">
                        <a href="/">
                            <img src="<?=asset('img/icon_7.png');?>" alt=""/>
                            <span>Lapki</span>
                        </a>
                    </div>
                    <div class="top_info d-none d-xl-inline-block">
                        №1 по поиску случек в Украине!
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-6 col-sm-6 px-xl-0 px-lg-0 px-md-3 px-sm-3">
                <div class="header-right">
                    <div class="profile d-none d-xl-inline-block">
                        <a href="#">
                            <i class="far fa fa-user"></i>
                            <span class="d-none d-xl-inline-block" style="position:relative;top:-5px">Мой профиль</span>
                        </a>
                    </div>

                    <div class="add_btn">
                        <button type="button" class="btn btn-primary d-none d-xl-inline-block" data-toggle="button" aria-pressed="false"
                                autocomplete="off">
                            <a href="#add"> <span class="fa fa-plus d-none d-xl-inline-block"> </span> Добавить питомца</a>
                        </button>

                        <button type="button" class="d-sm-inline-block d-md-inline-block d-lg-inline-block d-xl-none" data-toggle="button" aria-pressed="false"
                                autocomplete="off">
                            <a href="#"> <span class="fa fa-star"></span> </a>
                        </button>
                        <button type="button" class="d-sm-inline-block d-md-inline-block d-lg-inline-block d-xl-none" data-toggle="button" aria-pressed="false"
                                autocomplete="off">
                            <a href="#"> <span class="fa fa-plus"></span> </a>
                        </button>
                    </div>
                    <div class="profile d-xl-none">
                        <a href="#">
                            <i class="far fa fa-user"></i>
                            <span class="d-none d-xl-inline-block"></span>
                        </a>
                    </div>

                    <div class="lang d-none d-xl-inline-block ml-1">
                        <div class="dropdown d-none d-xl-inline-block ml-2">
                            <a class="btn btn-secondary dropdown-toggle switch-language" href="#" role="button" id="dropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                РУС
                            </a>

                            <div class="dropdown-menu dropdown-menu-list-language" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#">РУС</a>
                                <a class="dropdown-item" href="#">УКР</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="app" class="app-user-registered-block">
    @yield('content')
</div>

<footer id="footer" class="footer footer-not-home footer-small-height">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10">
                <nav class="footer-nav navbar">
                    <ul class="navbar-nav navbar flex-row">
                        <li class="nav-item">
                            <a href="{{ route( 'about_us' ) }}" class="">О Нас</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route( 'contacts' ) }}" class="">Контакт</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="">Релкама</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route( 'articles' ) }}" class="">Блог</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-12 col-lg-10 text-center d-xl-none d-lg-none">
                <div class="lang-footer-mobver">
                    <a href="#" class="active-lang-class">РУС</a>
                    <a href="#" class="">УКР</a>
                </div>
            </div>
            <div class="col-12 col-lg-2">
                <div class="socials">
                    <a href="#" target="_blank">
                        <img src="<?=asset('img/fb.png');?>">
                    </a>
                    <a href="#" target="_blank">
                        <img src="<?=asset('img/twt.png');?>">
                    </a>
                    <a href="#" target="_blank">
                        <img src="<?=asset('img/inst.png');?>">
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
