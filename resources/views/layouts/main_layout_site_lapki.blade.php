<!DOCTYPE html>
<?php
//dump($vars_for_template_view);
//dump($keywords);
//dump($meta_description);
//dump($title);
$current_route = Route::currentRouteName();
?>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="{{ (isset($meta_description)) ? $meta_description : '' }}">
    <meta name="keywords" content="{{ (isset($keywords)) ? $keywords : '' }}">
    <meta name="author" content="Lutskyi Yaroslav">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="<?=asset('favicon/favicon.ico');?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=asset('favicon/apple-touch-icon.png');?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=asset('favicon/favicon-32x32.png');?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=asset('favicon/favicon-16x16.png');?>">
    <link rel="manifest" href="<?=asset('favicon/site.webmanifest');?>">
    <link rel="mask-icon" href="<?=asset('favicon/safari-pinned-tab.svg');?>" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <title>{{ (isset($title)) ? $title : 'Site' }}</title> <!--вместо php-тегов в шаблонах `blade` используем удвоенные фигурные скобки -->

    {{--<script src="../../assets/js/ie-emulation-modes-warning.js"></script>--}}

    <link href="https://fonts.googleapis.com/css?family=Lato:400,500,600,900" rel="stylesheet" type="text/css">

    <link href="<?=asset('form-styler-plugin/dist/jquery.formstyler.css');?>" rel="stylesheet" />
    <link href="<?=asset('form-styler-plugin/dist/jquery.formstyler.theme.css');?>" rel="stylesheet" />

    <!--boot locale CSS-files wow-animation -->
    <link href="<?=asset('wow-animation/animate.min.css');?>" rel="stylesheet" media="screen">

        <!-- boot locale CSS-files of Bootstrap -->
    <link href="<?=asset('bootstrap/css/bootstrap.css');?>" rel="stylesheet" media="screen">
    <link href="<?=asset('bootstrap/css/bootstrap-grid.css');?>" rel="stylesheet" media="screen">
    <link href="<?=asset('bootstrap/css/bootstrap-reboot.css');?>" rel="stylesheet" media="screen">
        <!-- boot font-awesome Icons -->
    <link href="<?=asset('css/font-awesome.css');?>" rel="stylesheet">
        <!-- boot my custom main CSS-file -->
    <link href="<?=asset('css/app.css');?>" rel="stylesheet" type="text/css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?=asset('form-styler-plugin/dist/jquery.formstyler.js');?>" type="text/javascript"></script>
    <script>
        (function($) {
            $(function() {
                $('input, select').styler({ selectSearch: true, });
            });
        })(jQuery);
    </script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        section.home-filters { background: url(<?=asset('img/photobcg.png');?>) }
        section.home-filters.home-filters-search-result{ background: none !important;) }
        #footer nav::after { content: url(<?=asset('img/pets.png');?>) }
        input::-webkit-input-placeholder,textarea::-webkit-input-placeholder{color: #d5c2c2 !important;}
        input:-moz-placeholder,textarea:-moz-placeholder{color:#d5c2c2 !important;} /* Firefox 18- */
        input::-moz-placeholder,textarea::-moz-placeholder{color:#d5c2c2 !important;} /* Firefox 19+ */
        input:-ms-input-placeholder,textarea:-ms-input-placeholder{color:#d5c2c2!important;}
        section.about-us-section .about-us-info-wrap .col-about-us-info-bg {background: url(<?=asset('img/about-us-976@402.png');?>);}
        section.contacts_section .contact-info-wrap .col-contacts-info-bg {background: url(<?=asset('img/contact-page-photo.png');?>);}

        {{--section.home-filters-search-result #pet_type_input_search #pet_type-styler:before { background:url(<?=asset('img/shape_5@2x.png');?>); }--}}
        section.home-filters-search-result #pet_type_input_search.dogs #pet_type-styler:before { background:url(<?=asset('img/shape_5@2x.png');?>); }
        section.home-filters-search-result #pet_type_input_search.cats #pet_type-styler:before { background:url(<?=asset('img/shape_3@2x.png');?>); }
        section.home-filters-search-result #pet_type_input_search.rabbits #pet_type-styler:before { background:url(<?=asset('img/shape_15@2x.png');?>); }
        section.home-filters-search-result #pet_type_input_search.guinea-pigs #pet_type-styler:before { background:url(<?=asset('img/shape_222@2x.png');?>); }
        section.home-filters-search-result #pet_type_input_search.ferrets #pet_type-styler:before { background:url(<?=asset('img/shape_14@2x.png');?>); }
        section.home-filters-search-result #pet_type_input_search.chinchillas #pet_type-styler:before { background:url(<?=asset('img/shape_13@2x.png');?>); }
        section.home-filters-search-result #pet_type_input_search.birds #pet_type-styler:before { background:url(<?=asset('img/shape_12@2x.png');?>); }
        section.home-filters-search-result #pet_type_input_search.turtles #pet_type-styler:before { background:url(<?=asset('img/shape_11@2x.png');?>); }
        section.home-filters-search-result #pet_type_input_search.horses #pet_type-styler:before { background:url(<?=asset('img/shape_10@2x.png');?>); }

        section.header .right-part .header-right .pet-list-block .pet-item-link.pet-item-link-active:after { background:url(<?=asset('img/shape_pet_active.png');?>); }

        @media screen and (max-width: 399px) {
            .home-filters .main_filter .search input {font-size: 14px;}
        }
    </style>
</head>
<body data-spy="scroll" data-target=".navbar">

<!-- HEADER -->
<section id="header" class="header <?=($current_route!='home')?'header-not-home':'';?>">
    @yield('header')
</section>
<!-- /HEADER -->

<!-- CONTENT -->
{{--<div id="main_wrap_content" class="main-wrap-content">--}}
    @yield('content')
{{--</div>--}}
<!-- /CONTENT -->

<!-- FOOTER -->
<footer id="footer" class="footer <?=($current_route!='home')?'footer-not-home':'';?> <?=($current_route=='contacts')?'footer-small-height':'';?>">
    @yield('footer')
</footer>
<!-- /FOOTER -->


    <!-- boot locale jQuery library -->
<script src="<?= asset('js/jquery-3.3.1.min.js');?>" type="text/javascript"></script>
<!-- boot locale js-files wow-animation -->
<script src="<?= asset('wow-animation/wow.min.js');?>" type="text/javascript"></script>
    <!-- boot locale js-files Bootstrap -->
<script src="<?= asset('bootstrap/js/bootstrap.bundle.min.js');?>" type="text/javascript"></script>
    <!-- boot locale js-files Bootstrap -->
<script src="<?= asset('bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script>
    <!-- boot locale js-framework Vue.js-2 -->
<script src="<?= asset('vue2/vue.js');?>"></script>  <!-- подключ.`Vue.js 2`-->
    <!-- boot my custom main js-file -->
<script src="<?= asset('js/main.js');?>" type="text/javascript"></script>
<script>
    new WOW().init();
</script>
</body>
</html>